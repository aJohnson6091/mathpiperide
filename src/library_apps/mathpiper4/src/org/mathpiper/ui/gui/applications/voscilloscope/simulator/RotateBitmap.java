package org.mathpiper.ui.gui.applications.voscilloscope.simulator;

import java.awt.FontMetrics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.simulator:
//            StaticBitmapButton

public class RotateBitmap extends StaticBitmapButton
    implements MouseMotionListener
{

    private double values[];
    private int posValue;

    public RotateBitmap(String Files[], double Values[])
    {
        super(Files);
        this.values = Values;
        posValue = 0;
        addMouseMotionListener(this);
    }

    public double getValue()
    {
        return values[posValue];
    }

    public void setValue(double value)
    {
        boolean esta = false;
        int i = 0;
        do
        {
            if(i >= values.length || esta)
            {
                break;
            }
            if(value == values[i])
            {
                esta = true;
                break;
            }
            i++;
        } 
        while(true);
        
        if(esta)
        {
            posValue = i;
            super.posImage = i % super.Image.length;
        } else
        {
            System.out.println("There is no such value " + value + " in the component " + this);
        }
        repaint();
    }

    public int getPosition()
    {
        return posValue;
    }

    public void setPosition(int pos)
    {
        posValue = pos;
    }

    protected double getAngle(int x, int y, int centerX, int centerY)
    {
        double c2 = y - centerY;
        double c1 = centerX - x;
        double h = Math.sqrt(c1 * c1 + c2 * c2);
        if(h == (double)0)
        {
            return 0.0D;
        }
        if(y < centerY)
        {
            return 3.1415926535897931D + (3.1415926535897931D - Math.acos(c1 / h));
        } else
        {
            return Math.acos(c1 / h);
        }
    }

    protected int newPosition(int x, int y, int centerX, int centerY)
    {
        int posAnt = posValue % 44;
        double angulo = getAngle(x, y, centerX, centerY);
        int posNuevo = (int)(((double)22 * angulo) / 3.1415926535897931D);
        int inc = posNuevo - posAnt;
        if(inc > 22)
        {
            inc = -44 + inc;
        }
        if(inc < -22)
        {
            inc = 44 + inc;
        }
        return inc;
    }

    public void mouseClicked(MouseEvent e)
    {
        int x = e.getX();
        int y = e.getY();
        FontMetrics font = getFontMetrics(getFont());
        if(x >= 0 && x <= getSize().width && y >= 0 && y <= getSize().height)
        {
            int centerY;
            if(super.titulo.length() > 0)
            {
                centerY = 16 + font.getHeight();
            } else
            {
                centerY = 16;
            }
            int centerX = 32;
            posValue = posValue + newPosition(x, y, centerX, centerY);
            if(posValue < 0)
            {
                posValue = 0;
            } else
            if(posValue >= values.length)
            {
                posValue = values.length - 1;
            }
            super.posImage = posValue % super.Image.length;
            draw(getGraphics());
        }
    }

    public void mouseMoved(MouseEvent mouseevent)
    {
    }

    public void mouseDragged(MouseEvent e)
    {
        mouseClicked(e);
    }
}
