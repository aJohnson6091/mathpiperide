package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Meter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;

public class CircuitPiperMain extends JPanel {

    private CircuitPanel circuitPanel;

    public CircuitPiperMain() throws Exception {
        circuitPanel = new CircuitPanel();
        
        JDesktopPane desktopPane = new JDesktopPane();
        
        JInternalFrame internalFrame = new JInternalFrame();
        internalFrame.getContentPane().add(circuitPanel);
        internalFrame.setJMenuBar(createMenuBar());
        internalFrame.setResizable(true);
        internalFrame.setIconifiable(true);
        internalFrame.setMaximizable(true);
        internalFrame.pack();
        internalFrame.setTitle("CircuitPiper");
        internalFrame.setMaximum(true);
        internalFrame.setVisible(true);
        desktopPane.add(internalFrame);
        

        setLayout(new BorderLayout());
        add(desktopPane);
        //panel.add(createMenuBar(), BorderLayout.PAGE_START);
    }

    public JMenuBar createMenuBar() {

        JMenuBar menuBar = new JMenuBar();

        // ============================== File Menu
        JMenu fileMenu = new JMenu("File");

        JMenuItem importMenuItem = new JMenuItem("Import");
        importMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    JTextArea textArea = new JTextArea(20, 30);
                    textArea.setFont(new Font("monospaced", Font.PLAIN, 12));
                    
                    switch (JOptionPane.showConfirmDialog(null, new JScrollPane(textArea), "", JOptionPane.OK_CANCEL_OPTION)) {
                        case JOptionPane.OK_OPTION:
                            circuitPanel.circuit.readCircuit(textArea.getText());
                            break;
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        fileMenu.add(importMenuItem);

        JMenuItem exportMenuItem = new JMenuItem("Export");
        exportMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    JTextArea textArea = new JTextArea(20, 30);
                    textArea.setFont(new Font("monospaced", Font.PLAIN, 12));
                    textArea.append(circuitPanel.circuit.export());
                    JOptionPane.showMessageDialog(null, new JScrollPane(textArea), "", JOptionPane.OK_OPTION);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        fileMenu.add(exportMenuItem);

        JMenuItem clearMenuItem = new JMenuItem("Clear");
        clearMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                circuitPanel.circuit.clear();
            }
        });
        fileMenu.add(clearMenuItem);

        JMenuItem solverOptionsMenuItem = new JMenuItem("Numerical Solver Options");
        solverOptionsMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new ChangeErrorFrame(circuitPanel, 0, 0);
            }
        });
        fileMenu.add(solverOptionsMenuItem);

        JCheckBoxMenuItem cbMenuItem = new JCheckBoxMenuItem("Mark Capacitors and Inductors with +");
        cbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitPanel.drawingPanel.drawPlus = e.getStateChange() == ItemEvent.SELECTED;
                circuitPanel.drawingPanel.repaint();
            }
        });
        cbMenuItem.doClick();
        fileMenu.add(cbMenuItem);

        JCheckBoxMenuItem timeMenuItem = new JCheckBoxMenuItem("Time Passes");
        timeMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitPanel.timeOption = e.getStateChange() == ItemEvent.SELECTED;
                //circuitEnginePanel.dp.repaint();
            }
        });
        timeMenuItem.doClick();
        fileMenu.add(timeMenuItem);

        JMenuItem resetTimeMenuItem = new JMenuItem("Reset Time to 0s (Clears all Graphs)");
        resetTimeMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                synchronized (circuitPanel.drawingPanel) {
                    circuitPanel.circuit.time = 0;
                    circuitPanel.drawingPanel.repaint();
                    for (PhasePlane phasePlane : circuitPanel.phasePlanes) {
                        phasePlane.pb.clear(0);
                    }
                    for (Component ec : circuitPanel.circuit.electricComponents) {
                        if (ec.getClass() == CurrentIntegrator.class || ec.getClass() == Voltmeter.class
                                || ec.getClass() == Ammeter.class || ec.getClass() == VoltageIntegrator.class
                                || ec.getClass() == Ohmmeter.class || ec.getClass() == CapacitanceMeter.class
                                || ec.getClass() == InductanceMeter.class) {
                            if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                                ec.graphFrame.plotBox.clear(0);
                            }
                        }
                    }
                }
            }
        });
        fileMenu.add(resetTimeMenuItem);

        JMenuItem resetCompsMenuItem = new JMenuItem("Reset all Meters, Inductors and Capacitors");
        resetCompsMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                synchronized (circuitPanel.drawingPanel) {
                    for (Component ec : circuitPanel.circuit.electricComponents) {

                        ec.secondaryValue = 0;

                        if (ec instanceof Meter) {
                            ((Meter) ec).reset();
                        }
                    }
                    circuitPanel.drawingPanel.repaint();
                }
            }
        });

        JMenuItem aboutMenuItem = new JMenuItem("About");
        aboutMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "v.13\n"
                        + "Copyright \u00a9 2007-2008 Kevin Stueve kstueve@uw.edu.\n"
                        + "All rights reserved.\n"
                        + "IN NO EVENT SHALL KEVIN STUEVE BE LIABLE TO ANY PARTY\n"
                        + "FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES\n"
                        + "ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF\n"
                        + "KEVIN STUEVE HAS BEEN ADVISED OF THE POSSIBILITY OF\n"
                        + "SUCH DAMAGE.\n"
                        + "KEVIN STUEVE SPECIFICALLY DISCLAIMS ANY WARRANTIES,\n"
                        + "INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\n"
                        + "MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE\n"
                        + "PROVIDED HEREUNDER IS ON AN \"AS IS\" BASIS, AND KEVIN\n"
                        + "STUEVE HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,\n"
                        + "ENHANCEMENTS, OR MODIFICATIONS.\n"
                        + "CircuitPiper incorporates the Ptplot 5.7 plotting software, which is released\n"
                        + "under the UC Berkeley copyright.\n"
                        + "Copyright (c) 1995-2008 The Regents of the University of California.\n"
                        + "All rights reserved.\n"
                        + "IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY\n"
                        + "FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES\n"
                        + "ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF\n"
                        + "THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF\n"
                        + "SUCH DAMAGE.\n"
                        + "THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,\n"
                        + "INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\n"
                        + "MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE\n"
                        + "PROVIDED HEREUNDER IS ON AN \"AS IS\" BASIS, AND THE UNIVERSITY OF\n"
                        + "CALIFORNIA HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,\n"
                        + "ENHANCEMENTS, OR MODIFICATIONS.",
                        "About CircuitPiper",
                        JOptionPane.PLAIN_MESSAGE);

            }
        });

        fileMenu.add(resetCompsMenuItem);
        fileMenu.add(aboutMenuItem);

        JMenuItem saveMenuItem = new JMenuItem();
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JFileChooser saveCircuitFileChooser = new JFileChooser();

                FileFilter filter = new FileNameExtensionFilter("Serialzed circuit file", "ser");
                saveCircuitFileChooser.addChoosableFileFilter(filter);

                int returnValue = saveCircuitFileChooser.showSaveDialog(null);

                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File circuitFile = saveCircuitFileChooser.getSelectedFile();
                    try {
                        circuitPanel.circuit.save(circuitFile.getCanonicalPath());
                    } catch (java.io.IOException ioe) {
                        ioe.printStackTrace();
                    }

                }

            }
        });
        fileMenu.add(saveMenuItem);

        JMenuItem openMenuItem = new JMenuItem();
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {

                JFileChooser openCircuitFileChooser = new JFileChooser();

                //File file = new File("/home/tkosan/bat_and_two_resistors.ser");// todo:tk
                //openCircuitFileChooser.setSelectedFile(file);
                FileFilter filter = new FileNameExtensionFilter("Serialzed circuit file", "ser");
                openCircuitFileChooser.addChoosableFileFilter(filter);

                int returnValue = openCircuitFileChooser.showOpenDialog(null);

                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File circuitFile = openCircuitFileChooser.getSelectedFile();
                    try {
                        circuitPanel.isRunning = false;
                        circuitPanel.circuit.open(circuitFile.getCanonicalPath());

                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    circuitPanel.circuit.updateCircuit();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //circuitEnginePanel.isRunning = true;
                                //circuitEnginePanel.setIsDrawing(true);
                                circuitPanel.setHintDrawing();

                                circuitPanel.repaint();

                                circuitPanel.waiting = false;
                            }
                        });
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        });
        fileMenu.add(openMenuItem);

        menuBar.add(fileMenu);

        // ============================== Board Menu
        JMenu boardMenu = new JMenu("Board");

        /*
        JCheckBoxMenuItem boardOutlineMenuItem = new JCheckBoxMenuItem("Board Outline");
        boardOutlineMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitEnginePanel.drawingPanel.isShowBoardOutline = e.getStateChange() == ItemEvent.SELECTED;
                circuitEnginePanel.drawingPanel.repaint();
            }
        });
        boardMenu.add(boardOutlineMenuItem);
         */
        JRadioButtonMenuItem rbMenuItem;
        ButtonGroup group = new ButtonGroup();

        rbMenuItem = new JRadioButtonMenuItem("None");
        rbMenuItem.setSelected(true);
//rbMenuItem.setMnemonic(KeyEvent.VK_R);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitPanel.drawingPanel.isShowBoardOutline = false;
                circuitPanel.drawingPanel.repaint();
            }
        });
        group.add(rbMenuItem);
        boardMenu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("6\" x 6\"");
        rbMenuItem.setSelected(false);
//rbMenuItem.setMnemonic(KeyEvent.VK_R);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitPanel.drawingPanel.boardXPixels = 6 * 72;
                circuitPanel.drawingPanel.boardYPixels = 6 * 72;
                circuitPanel.drawingPanel.isShowBoardOutline = true;
                circuitPanel.drawingPanel.repaint();
            }
        });
        group.add(rbMenuItem);
        boardMenu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("8\" x 5.5\"");
//rbMenuItem.setMnemonic(KeyEvent.VK_O);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                circuitPanel.drawingPanel.boardXPixels = 8 * 72;
                circuitPanel.drawingPanel.boardYPixels = 5.5 * 72;
                circuitPanel.drawingPanel.isShowBoardOutline = true;
                circuitPanel.drawingPanel.repaint();
            }
        });
        group.add(rbMenuItem);
        boardMenu.add(rbMenuItem);

        menuBar.add(boardMenu);

        // ============================== Misc Menu
        JMenu miscMenu = new JMenu("Misc");

        JMenuItem autoLayoutMenuItem = new JMenuItem("Auto Layout");
        autoLayoutMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                circuitPanel.circuit.viewAutoLayout();
            }
        });
        miscMenu.add(autoLayoutMenuItem);

        JMenuItem exampleMenuItem = new JMenuItem("Example");
        exampleMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    circuitPanel.circuit.exampleCircuit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        miscMenu.add(exampleMenuItem);

        JMenuItem imageMenuItem = new JMenuItem("Image");
        imageMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Utility.saveImageOfComponent(circuitPanel.screenCapturePanel);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        miscMenu.add(imageMenuItem);

        JMenuItem printMenuItem = new JMenuItem("Print");
        printMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    circuitPanel.print();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        miscMenu.add(printMenuItem);

        menuBar.add(miscMenu);

        return menuBar;
    }

    public CircuitPanel getPanel() {
        return circuitPanel;
    }

    /*
    public static void main(String[] args) throws Exception {

        CircuitPiperMain circuitPiperMain = new CircuitPiperMain();

        JFrame frame = new javax.swing.JFrame();

        frame.setBackground(Color.WHITE);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (frame.getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE) {
                    System.exit(0);
                } else if (frame.getDefaultCloseOperation() == JFrame.DISPOSE_ON_CLOSE) {
                    circuitPiperMain.getPanel().isRunning = false;
                    circuitPiperMain.getPanel().myTimer.stop();
                    frame.dispose();
                }
            }
        });

        frame.getContentPane().add(circuitPiperMain.getPanel());

        frame.setJMenuBar(circuitPiperMain.createMenuBar());

        frame.pack();

        frame.setTitle("CircuitPiper");
        frame.setSize(new Dimension(1400, 800));
        //frame.setResizable(false);
        //frame.setPreferredSize(new Dimension(700, 700));
        frame.setLocationRelativeTo(null); // added

        frame.setVisible(true);
    }
*/
        public static void main(String[] args) throws Exception {
            
        final CircuitPiperMain circuitPiperMain = new CircuitPiperMain();

        final JFrame frame = new javax.swing.JFrame();

        frame.setBackground(Color.WHITE);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (frame.getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE) {
                    System.exit(0);
                } else if (frame.getDefaultCloseOperation() == JFrame.DISPOSE_ON_CLOSE) {
                    circuitPiperMain.getPanel().isRunning = false;
                    circuitPiperMain.getPanel().myTimer.stop();
                    frame.dispose();
                }
            }
        });

        frame.getContentPane().add(circuitPiperMain);
        
        //frame.setSize(new Dimension(1400, 800));
        
        frame.pack();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(width/2, height/2);
        frame.setLocationRelativeTo(null); // added

        frame.setVisible(true);
    }
}
