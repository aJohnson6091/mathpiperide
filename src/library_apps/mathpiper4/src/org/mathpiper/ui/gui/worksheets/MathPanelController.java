package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.mathpiper.ui.gui.worksheets.symbolboxes.SymbolBox;

public class MathPanelController extends JPanel implements ItemListener {

    private JSlider scaleSlider;
    protected ViewPanel viewPanel;

    public MathPanelController(final ViewPanel viewPanel, double initialValue) {
        super();
        this.viewPanel = viewPanel;

        scaleSlider = new JSlider(JSlider.HORIZONTAL, 1, 100, (int) (initialValue * 10));
        scaleSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                JSlider source = (JSlider) e.getSource();
                //if (!source.getValueIsAdjusting()) {
                int intValue = (int) source.getValue();
                double doubleValue = intValue / 10.0;
                //System.out.println("XXX: " + doubleValue);
                viewPanel.setViewScale(doubleValue);
                viewPanel.repaint();
            }
        });

        //Turn on labels at major tick marks.
        //framesPerSecond.setMajorTickSpacing(10);
        //framesPerSecond.setMinorTickSpacing(1);
        //framesPerSecond.setPaintTicks(true);
        scaleSlider.setPaintLabels(true);

        JPanel sliderPanel = new JPanel();
        sliderPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        sliderPanel.add(new JLabel("Resize"));
        sliderPanel.add(scaleSlider);
        this.add(sliderPanel);

        /* 
         * This code is only needed to debug the Yacas math renderer code.
        JCheckBox drawBoundingBoxCheckBox = new JCheckBox("Draw Bounding Boxes");

        drawBoundingBoxCheckBox.setSelected(SymbolBox.isDrawBoundingBox());

        drawBoundingBoxCheckBox.addItemListener(this);

        this.add(drawBoundingBoxCheckBox);
         */
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            SymbolBox.setDrawBoundingBox(true);
            viewPanel.repaint();

        } else {
            SymbolBox.setDrawBoundingBox(false);
            viewPanel.repaint();
        }

    }//end method.
}
