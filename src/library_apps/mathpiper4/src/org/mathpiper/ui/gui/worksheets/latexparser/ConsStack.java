
package org.mathpiper.ui.gui.worksheets.latexparser;

import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.ui.gui.worksheets.symbolboxes.SymbolName;


public class ConsStack implements TexStack
{
      Cons[] stack = new Cons[1024];

    int stackDepth = 0;


    @Override
    public Object pop() {
        stackDepth--;

        Cons result = stack[stackDepth];

        return result;
    }

    public void push(Object aSbox) {

        stack[stackDepth] = (Cons) aSbox;

        stackDepth++;
    }



    @Override
    public void process(String aType) throws Throwable {

        if (aType.equals("=") || 
                aType.equals("\\neq") || 
                aType.equals("\\leq") || 
                aType.equals("\\geq") || 
                aType.equals("+") || 
                aType.equals(",") || 
                aType.equals("\\wedge") || 
                aType.equals("\\vee") || 
                aType.equals("<") || 
                aType.equals(">") || 
                aType.equals("<=") || 
                aType.equals(">=")
                
                ) {

            if (aType.equals("="))
            {
                aType = "==";
            }
            else if (aType.equals("\\neq"))
            {
                aType = "!=?";
            }
            else if (aType.equals("<"))
            {
                aType = "<?";
            }
            else if (aType.equals(">"))
            {
                aType = ">?";
            }
            else if (aType.equals("\\leq"))
            {
                aType = "<=?";
            }
            else if (aType.equals("\\geq"))
            {
                aType = ">=?";
            }
            
            Cons right = (Cons) pop();
            Cons left = (Cons) pop();
            //push(new InfixOperator((SymbolBox)left, new SymbolName(aType), (SymbolBox)right));
            
            Cons headCons = AtomCons.getInstance(10, aType);
            headCons.setCdr(left);
            left.setCdr(right);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
            
        } else if (aType.equals("/") || aType.equals("\\div")) {

            if(aType.equals("\\div"))
            {
                aType = "/";
            }
            Cons denom = (Cons) pop();
            Cons numer = (Cons) pop();
            //push(new Fraction((SymbolBox)numer, (SymbolBox)denom));
            
            Cons headCons = AtomCons.getInstance(10, aType);
            headCons.setCdr(numer);
            numer.setCdr(denom);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);           
            
        } else if (aType.equals("-/2")) {

            Cons right = (Cons) pop();
            Cons left = (Cons) pop();
            //push(new InfixOperator((SymbolBox)left, new SymbolName("-"), (SymbolBox)right));
            
            Cons headCons = AtomCons.getInstance(10, "-");
            headCons.setCdr(left);
            left.setCdr(right);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
        } else if (aType.equals("-/1")) {

            Cons right = (Cons) pop();
            //push(new PrefixOperator(new SymbolName("-"), (SymbolBox)right));
            
            Cons headCons = AtomCons.getInstance(10, "-");
            headCons.setCdr(right);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
       /* } else if (aType.equals("~")) {

            Cons right = (Cons) pop();
            push(new PrefixOperator(new SymbolName("~"), (SymbolBox)right));
        } else if (aType.equals("!")) {

            Cons left = (Cons) pop();
            push(new PrefixOperator((SymbolBox)left, new SymbolName("!")));*/
        } else if (aType.equals("*")) {

            Cons right = (Cons) pop();
            Cons left = (Cons) pop();
            //push(new InfixOperator((SymbolBox)left, new SymbolName("*"), (SymbolBox)right));
            
            Cons headCons = AtomCons.getInstance(10, aType);
            headCons.setCdr(left);
            left.setCdr(right);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
        /*} else if (aType.equals("[func]")) {

            Cons right = (Cons) pop();
            Cons left = (Cons) pop();
            push(new PrefixOperator((SymbolBox)left, (SymbolBox)right)); */
        } else if (aType.equals("^")) {
            
            /*
            todo:tk:this code needs to be generalized.
            ParseLatex("2^3^4^5*6") 
            evaluates to 
            2^((3^4)^5)*6
            which differs from 
            2^(3^(4^5))*6
            */

            Cons right = (Cons) pop();
            Cons left = (Cons) pop();
            
            
            if( left.car() instanceof AtomCons  && ((String)Cons.caar(left)).equals("^"))
            {
                Cons leftsRightOperator = (Cons)Cons.cddar(left);
                
                Cons headCons = AtomCons.getInstance(10, aType);
                ((Cons)Cons.cdar(left)).setCdr(SublistCons.getInstance(headCons));
                headCons.setCdr(leftsRightOperator);
                leftsRightOperator.setCdr(right);
                Cons operatorCons = left;
                push(operatorCons);
            
            } else {
                //push(new SuperSubFix((SymbolBox)left, (SymbolBox)right, null));
                
                Cons headCons = AtomCons.getInstance(10, aType);
                headCons.setCdr(left);
                left.setCdr(right);
                Cons operatorCons = SublistCons.getInstance(headCons);
                push(operatorCons);
            } 
            

            
       /* } else if (aType.equals("_")) {

            Cons right = (Cons) pop();
            Cons left = (Cons) pop();

            if (left instanceof SuperSubFix) {

                SuperSubFix sbox = (SuperSubFix) left;
                sbox.setSubfix((SymbolBox)right);
                push(sbox);
            } 
            else {
                push(new SuperSubFix((SymbolBox)left, null, (SymbolBox)right));
            }*/
        } else if (aType.equals("[sqrt]")) {

            Cons left = (Cons) pop();
            //push(new SquareRoot((SymbolBox)left));
            
            Cons headCons = AtomCons.getInstance(10, "Sqrt");
            headCons.setCdr(left);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
        /*} else if (aType.equals("[sum]")) {
            push(new Sum());
        } else if (aType.equals("[int]")) {
            push(new Integral());*/
        } else if (aType.equals("[pi]")) {
            Cons headCons = AtomCons.getInstance(10, "Pi");
            push(headCons);
            
        }else if (aType.equals("[absBracket]")) {

            Cons left = (Cons) pop();
            //push(new SquareRoot((SymbolBox)left));
            
            Cons headCons = AtomCons.getInstance(10, "Abs");
            headCons.setCdr(left);
            Cons operatorCons = SublistCons.getInstance(headCons);
            push(operatorCons);
            
        /*} else if (aType.equals("[sum]")) {
            push(new Sum());
        } else if (aType.equals("[int]")) {
            push(new Integral());*/
        } else if (aType.equals("[roundBracket]")) {

            Cons left = (Cons) pop();
            //push(new Bracket((SymbolBox)left, "(", ")"));
            
            push(left);
       /* } else if (aType.equals("[squareBracket]")) {

            Cons left = (Cons) pop();
            push(new Bracket((SymbolBox)left, "[", "]"));
        } else if (aType.equals("[accoBracket]")) {

            Cons left = (Cons) pop();
            push(new Bracket((SymbolBox)left, "{", "}"));
        } else if (aType.equals("[grid]")) {

            Cons widthBox = (Cons) pop();
            Cons heightBox = (Cons) pop();
            int width = Integer.parseInt(((SymbolName) widthBox).iSymbol);
            int height = Integer.parseInt(((SymbolName) heightBox).iSymbol);
            Grid grid = new Grid(width, height);
            int i;
            int j;

            for (j = height - 1; j >= 0; j--) {

                for (i = width - 1; i >= 0; i--) {

                    Cons value = (Cons) pop();
                    grid.setSBox(i, j, (SymbolBox)value);
                }
            }

            push(grid); */
        } else {
            push(AtomCons.getInstance(10, aType));
        }
    }

    @Override
    public void processLiteral(String aExpression) {
        push(new SymbolName(aExpression));
    }  
}
