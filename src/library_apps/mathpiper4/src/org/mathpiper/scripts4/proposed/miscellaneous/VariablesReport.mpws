%mathpiper,def="VariablesReport"

RulebaseEvaluateArguments("VariablesReport", ["firstParameter"]);
RulebaseListedEvaluateArguments("VariablesReport", ["firstParameter", "parametersList"]);
UnFence("VariablesReport", 1);
UnFence("VariablesReport", 2);
HoldArgument("VariablesReport", "firstParameter");
HoldArgument("VariablesReport", "parametersList");

//Handle single argument.
5 ## VariablesReport(firstParameter_) <-- `VariablesReport( @firstParameter, []);


//Handle two arguments because the second argument does not come in a list for some reason.
15 ## VariablesReport(firstParameter_, parameterList_Atom?) <-- `VariablesReport( @firstParameter, [@parameterList]);


//Main routine.  It will automatically accept 3 or more arguments calls because the
//arguments after the first and a second come in a list.
20 ## VariablesReport(firstParameter_, parametersList_List?) <--
{
    Local(word, words, index, value);
  
    firstParameter := ObjectToMeta(firstParameter);

    parametersList := ObjectToMeta(parametersList, Procedures?:False);

    variableNames := Concat([firstParameter], Eval(parametersList));


    ForEach(variable, variableNames)
    {
        If(UnderscoreConstant?(variable))
        {
            variable := ToString(MetaToObject(variable));
        }
        Else
        {
            variable := ToString(variable);
        }
    
        words := StringSplit(variable, "(?=\\p{Upper})");
        
        words := MapSingle("StringLowerCase", words);
        
        index := 1;
        While(index <? Length(words))
        {
            word := words[index];
            
            WriteString(Decide(index =? 1, ListToString(Replace!(StringToList(words[1]),1,StringUpperCase(words[1][1])), ""), word,)); 
            
            If(words[index+1] !=? "dollars")
            {
                WriteString(" ");
            }
            index++;
        }

        word := words[index];
        If(word !=? "dollars")
        {
            WriteString(Decide(index =? 1, ListToString(Replace!(StringToList(words[1]),1,StringUpperCase(words[1][1])), ""), word,));
        }

        WriteString(": ");
        
        
        value := Eval(ToAtom(variable));
        
        If(StringEndsWith?(word,"dollars"))
        {
            WriteString("$");
        }

        If(Decimal?(value))
        {
            Write(RoundToPlace(value, 2));
        }
        Else
        {
            Write(Eval(value));
        }
        
        WriteString(Nl());
    }
}

%/mathpiper





%mathpiper_docs,name="VariablesReport",categories="Programming Procedures,Input/Output"
*CMD VariablesReport --- output a description of the passed-in variables along with their values 
*CALL
    VariablesReport(var1, var2, ...)

    
*DESC
This procedure is used to generate a quick report that consists of the names
of variables separated into separate words along with the values of these variables.
The output is provided as a side effect.

*PARMS
{var} -- the name of a variable (either within or not within double quotes)

*E.G.
In> numberOfPeople := 4
Result: 4

In> costPerPerson := 8
Result: 8

In> VariablesReport(numberOfPeople, costPerPerson)
Result: True
Side Effects:
Number of people: 4
Cost per person: 8

%/mathpiper_docs
