%mathpiper,def="Mark"

Mark(expression, position, pattern, color) :=
{
    Local(list);
    
    list := [
        ["function",
            Lambda([trackingList,positionString,node], 
            {
                MetaSet(node, "HighlightColor", color);
                MetaSet(node, "HighlightNodeShape", "RECTANGLE");
            })
        ]
    ];
    
    TreeProcess(expression, pattern, list, Position:position);
}

%/mathpiper





%mathpiper_docs,name="Mark",categories="Programming Procedures,Expression Trees"
*CMD Mark --- highlights a subtree of an expression tree at the given position if it matches the given pattern

*CALL
    Mark(expression, position, pattern, color)

*PARMS

{expression} -- expression tree to be highlighted

{position} -- position of the subtree

{pattern} -- pattern to match against subtrees

{color} -- the highlighting color

*DESC

Highlights a subtree of an expression tree at the
given position if it matches the given pattern.
Highlighting metadata is only applied to the
dominant node because the tree viewer will
highlight a node's subtree if its dominant node is
highlighted.

*E.G.
In> result := Mark('(1+2+3), "1", a_ + b_, "BLUE")
Result: (1 + 2) + 3

In> MetaEntries(result[1])
Result: ["HighlightNodeShape":"RECTANGLE","HighlightColor":"BLUE"]

*SEE HighlightPattern, MarkAll
%/mathpiper_docs





%mathpiper,name="Mark",subtype="automatic_test"

{
    Local(result);
    
    result := Mark('(1+2+3), "1", a_ + b_, "BLUE");
    
    Verify(MetaGet(result[1], "HighlightColor"), "BLUE");
}

%/mathpiper