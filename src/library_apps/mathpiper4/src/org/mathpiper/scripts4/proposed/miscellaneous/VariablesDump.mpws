%mathpiper,def="VariablesDump"

RulebaseEvaluateArguments("VariablesDump", ["firstParameter"]);
RulebaseListedEvaluateArguments("VariablesDump", ["firstParameter", "parametersList"]);
UnFence("VariablesDump", 1);
UnFence("VariablesDump", 2);
HoldArgument("VariablesDump", "firstParameter");
HoldArgument("VariablesDump", "parametersList");

//Handle single argument.
5 ## VariablesDump(firstParameter_) <-- `VariablesDump( @firstParameter, []);


//Handle two arguments because the second argument does not come in a list for some reason.
15 ## VariablesDump(firstParameter_, parameterList_Atom?) <-- `VariablesDump( @firstParameter, [@parameterList]);


//Main routine.  It will automatically accept 3 or more arguments calls because the
//arguments after the first and a second come in a list.
20 ## VariablesDump(firstParameter_, parametersList_List?) <--
{
    Local(word, words, index, value);
  
    firstParameter := ObjectToMeta(firstParameter);

    parametersList := ObjectToMeta(parametersList, Procedures?:False);

    variableNames := Concat([firstParameter], Eval(parametersList));


    ForEach(variable, variableNames)
    {
        If(UnderscoreConstant?(variable))
        {
            variable := ToString(MetaToObject(variable));
        }
        Else
        {
            variable := ToString(variable);
        }
    
        WriteString(variable);
        WriteString(": ");
        WriteString(ToString(Eval(ToAtom(variable))));
        
        WriteString(Nl());
    }
}

%/mathpiper

    %output,mpversion=".262",preserve="false"
      Result: True
.   %/output





%mathpiper_docs,name="VariablesDump",categories="Programming Procedures,Input/Output"
*CMD VariablesDump --- output the names of the passed-in variables along with their values 
*CALL
    VariablesDump(var1, var2, ...)

    
*DESC
This procedure is used to generate a dump of the specified variables.

*PARMS
{var} -- the name of a variable (either within or not within double quotes)

*E.G.
In> numberOfPeople := 4
Result: 4

In> costPerPerson := 8
Result: 8

In> VariablesDump(numberOfPeople, costPerPerson)
Result: True
Side Effects:
numberOfPeople: 4
costPerPerson: 8

%/mathpiper_docs
