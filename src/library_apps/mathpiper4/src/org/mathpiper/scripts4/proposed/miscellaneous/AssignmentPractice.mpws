%mathpiper,def="AssignmentPractice"

AssignmentPractice(numberOfVariables, correctInARow, numberOfReassignments, variableRange, includeIfProcedure?, includeElse?) :=
{
    //numberOfVariables := 2; correctInARow := 2; numberOfReassignments := 1; variableRange := 0 .. 9; includeIfProcedure? := True;

    Check(Boolean?(includeIfProcedure?), "The argument includeIfFunction must be True or False.");

    //todo:tk:make a,b,etc. local in a loop.
    Local(a,b,c,d,e,f,actualAnswer,allCorrectFlag,allVariables,anAssignment, answerPanel,booleanOperator,currentCorrectInARow,jPanel,label,main,mainLoopMessageLabel,mainLoopMessageString,questionString,questionTextArea,questionTime,reassignmentsIndex,result,textField,textFieldsList,totalTime,userAnswer1,userAnswer2,value,variable1Name,variable2,variable2Name,variable3Name,variable4Name,variable,variableCombination,variableCombinations,variableNames,variablesIndex,variableValues,wrongCount,scrollPane);
    
    currentCorrectInARow := 0;
    
    wrongCount := 0;

    main := None;
    
    WriteString("[" + Nl());
    
    totalTime := Time() Until(currentCorrectInARow =? correctInARow |? main =? JavaAccess("javax.swing.JOptionPane","CANCEL_OPTION"))
    {
    
        jPanel := JavaNew("javax.swing.JPanel");
        
        //this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        
        //JavaCall(jPanel, "setLayout", JavaNew("javax.swing.BoxLayout", jPanel, JavaCall("javax.swing.BoxLayout","Y_AXIS")));
        
        allVariables := BuildList(UnicodeToString(x),x,97,97+numberOfVariables,1);
        
        variableNames := Take(allVariables, numberOfVariables);
        
        variableValues := Take(Shuffle(variableRange), numberOfVariables);
        
        textFieldsList := [];
        
        variablesIndex := 1;
        
        questionString :=  "What values are assigned to these variables" + Nl() + "after the following code is evaluated?" + Nl() + Nl();
    
    
        questionTextArea := JavaNew("javax.swing.JTextArea", questionString, 25, 40);

        
        JavaCall(questionTextArea, "setEditable", False);
        
        scrollPane := JavaNew("javax.swing.JScrollPane", questionTextArea, JavaCall("javax.swing.JScrollPane", "VERTICAL_SCROLLBAR_ALWAYS"), JavaCall("javax.swing.JScrollPane", "HORIZONTAL_SCROLLBAR_AS_NEEDED"));
        
        JavaCall(jPanel, "add", scrollPane);
        
        answerPanel := JavaNew("javax.swing.JPanel");
             
        //Create initial variable assignments.
        While(variablesIndex <=? numberOfVariables)
        {
            variable := variableNames[variablesIndex];
            
            value := variableValues[variablesIndex];
            
            `Assign(@variable, value);
            
            textField := JavaNew("javax.swing.JTextField", 2);
            
            textFieldsList[variable] := textField;
            
            label := JavaNew("javax.swing.JLabel", variable);
            
            JavaCall(answerPanel, "add", label);
            
            JavaCall(answerPanel, "add", textField);
            
            JavaCall(answerPanel, "add", JavaCall("javax.swing.Box","createHorizontalStrut", 15));
            
            anAssignment := variable + " := " + ToString(value) + ";";
            
            Write(JavaAccess(JavaNew("java.lang.String", anAssignment), "replaceAll", "\\n", "|"));
            
            Write(',);
            
            JavaCall(questionTextArea, "append", anAssignment + Nl());
            
            variablesIndex++;
        }
        
        JavaCall(questionTextArea, "append", Nl());
        
        
        
        //Create variable reassignments.
        reassignmentsIndex := 1;
        While(reassignmentsIndex <=? numberOfReassignments)
        {
            variableCombinations := Shuffle(CombinationsList(Shuffle(variableNames),2));
            variableCombination := Shuffle(PopFront(variableCombinations));
            variable1Name := variableCombination[1];
            variable2Name := variableCombination[2];
            
            
            anAssignment := variable1Name + " := " + variable2Name + ";";
            
            If(includeIfProcedure? =? True)
            {
                variableCombinations := Shuffle(CombinationsList(Shuffle(variableNames),2));
                variableCombination := Shuffle(PopFront(variableCombinations));
                variable3Name := variableCombination[1];
                variable4Name := variableCombination[2];
                
                booleanOperator := RandomPick(["<?","<=?",">?",">=?","=?","!=?"]);
                
                
                anAssignment := "If(" + ToString(variable3Name) + " " + booleanOperator + " " + ToString(variable4Name) + ")" + Nl() + "{" + Nl() + "    " + anAssignment + Nl() + "}";
                
                If(includeElse? =? True)
                {
                
                    variableCombinations := Shuffle(CombinationsList(Shuffle(variableNames),2));
                    variableCombination := Shuffle(PopFront(variableCombinations));
                    variable3Name := variableCombination[1];
                    variable4Name := variableCombination[2];
                    
                    anAssignment := anAssignment + Nl() + "Else" + Nl() + "{" + Nl() + "    " + variable3Name + " := " + variable4Name + ";" + Nl() + "}" + Nl();
                                   
                }
                Else
                {
                    anAssignment := anAssignment + ";" + Nl();
                }
                
            }
            
            Write(JavaAccess(JavaNew("java.lang.String", anAssignment), "replaceAll", "\\n", "|"));
            
            Write(',);
            
            JavaCall(questionTextArea, "append", anAssignment + Nl());
            
            Eval(PipeFromString(anAssignment) ParseMathPiper());
            
            reassignmentsIndex++;
        }
        
    //SysOut(ToString(a) + ", " + ToString(b) + ", " + ToString(c));     
    
        JavaCall(jPanel, "add", answerPanel);
        
        allCorrectFlag := False;
        
        result := None;
        
        
        WriteString("[");
        questionTime := Time() Until(allCorrectFlag =? True |? result =? JavaAccess("javax.swing.JOptionPane","CANCEL_OPTION"))
        {
            allCorrectFlag := True;
                
            //result := JavaAccess("javax.swing.JOptionPane","showConfirmDialog",Null, jPanel, "Question", JavaCall("javax.swing.JOptionPane","OK_CANCEL_OPTION"));
            
            result := main := AskUser(jPanel);
            
            If(result !=? JavaAccess("javax.swing.JOptionPane","CANCEL_OPTION"))
            {
                ForEach(variableName, variableNames)
                {
                    userAnswer1 := JavaAccess(textFieldsList[variableName], "getText");
                    
                    userAnswer2 := Decide(userAnswer1 =? "", None, ToAtom(userAnswer1));
                    
                    actualAnswer := Eval(ToAtom(variableName));
                    
                    If(userAnswer2 !=? actualAnswer) (allCorrectFlag := False);
                    
                    WriteString("[");
                    Write(variableName, ', , actualAnswer, ', , userAnswer2);
                    WriteString("],");
                }
                     
                
                If(allCorrectFlag =? False &? result !=? JavaAccess("javax.swing.JOptionPane","CANCEL_OPTION"))
                {   
                    TellUser("Incorrect. Try again.");
                    
                    currentCorrectInARow := 0;
                    
                    wrongCount++;
                }
            
            }
            Else
            {
                currentCorrectInARow--;
            }
        
        }
        
        Write(questionTime);
        WriteString("],");
        WriteString(Nl());
        
        
        currentCorrectInARow++;
        
        
        If(correctInARow !=? currentCorrectInARow &? main !=? JavaAccess("javax.swing.JOptionPane","CANCEL_OPTION"))
        {
        
            mainLoopMessageString := PatchString(
            "<html>
            You have {?Write(currentCorrectInARow);?} correct in a row.<br /><br />
            Your goal is to get {?Write(correctInARow);?} in a row.<br /><br />
            Press <b>OK</b> to keep going or <b>Cancel</b> to end.
            
            </html>");
            mainLoopMessageLabel := JavaNew("javax.swing.JLabel", mainLoopMessageString);   

            //main := JavaAccess("javax.swing.JOptionPane","showConfirmDialog",Null, mainLoopMessageLabel, "Question", JavaCall("javax.swing.JOptionPane","OK_CANCEL_OPTION"));
            
            main := AskUser(mainLoopMessageLabel);
        }
        Else
        {
            TellUser("You answered " + ToString(currentCorrectInARow) + " in a row!");
        }
            
            
    }
    
    WriteString("]");
    

    Echo("Number Of Variables: ", numberOfVariables);
    Echo("Number Of Reassignments: ", numberOfReassignments);
    Echo("Variables Range: ", variableRange);
    Echo("Correct In A Row: ", currentCorrectInARow);
    Echo("Correct In A Row Needed: ", correctInARow);
    Echo("Total Time: ", totalTime);
    Echo("Wrong Count: ", wrongCount);

}

%/mathpiper







%mathpiper,title="",name="practice1",preserve_output="false",log="false"

AssignmentPractice(3,2,3,0 .. 9, True, True);

%/mathpiper




%mathpiper,title="",name="practice1",preserve_output="false",log="false"

AssignmentPractice(3,2,6,0 .. 9, True, True);

%/mathpiper









