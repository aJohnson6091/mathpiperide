%mathpiper,def="SymbolPathDistanceTotal"

SymbolPathDistanceTotal(expression, symbol) :=
{
    Local(positions, combinations, totalDistance);
    
    positions := PositionsPattern(expression, symbol);
    
    If(Length(positions) <? 2)
    {
        totalDistance := 0;
    }
    Else
    {
        combinations := CombinationsList(positions, 2);
        
        totalDistance := 0;
        
        ForEach(pair, combinations)
        {
            totalDistance +:= NodePathDistance(pair[1], pair[2]);
        }
    }
    
    totalDistance;
}

%/mathpiper





%mathpiper_docs,name="SymbolPathDistanceTotal",categories="Programming Procedures,Expression Trees"
*CMD SymbolPathDistanceTotal --- determine the total path distance between all copies of a specified symbol

*CALL
    SymbolPathDistanceTotal(expression, symbol)

*PARMS

{expression} -- an expression

{symbol} -- a symbol that is present in the expression in 0 or more places

*DESC

Determine the total path distance between all copies of a specified symbol.

*E.G.
In> SymbolPathDistanceTotal('( a+b/c-x*d+a/e), 'a)
Result: 5

*SEE NodePathDistance
%/mathpiper_docs





%mathpiper,name="SymbolPathDistanceTotal",subtype="automatic_test"

Verify(SymbolPathDistanceTotal('( a + b/c - x*d + a/e), 'a), 5);

%/mathpiper

