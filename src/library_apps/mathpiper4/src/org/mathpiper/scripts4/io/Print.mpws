%mathpiper,def="Print;PrintArg;PrintArgComma;PrintArgBlock"


/* A reference print implementation. Expand at own leisure.
 *
 * This file implements Print, a scripted expression printer.
 */


/* 60000 is the maximum precedence allowed for operators */
10 ## Print(x_) <--
{
  Print(x,60000);
  NewLine();
  DumpErrors();
}

/* Print an argument within an environment of precedence n */
10 ## Print(x_Atom?,n_) <-- Write(x);
10 ## Print(x_,n_)::(Infix?(Type(x))&? ArgumentsCount(x) =? 2) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Decide(bracket,WriteString("("));
  Print(x[1],LeftPrecedenceGet(Type(x)));
  Write(x[0]);
  Print(x[2],RightPrecedenceGet(Type(x)));
  Decide(bracket,WriteString(")"));
}

10 ## Print(x_,n_)::(Prefix?(Type(x)) &? ArgumentsCount(x) =? 1) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Write(x[0]);
  Decide(bracket,WriteString("("));
  Print(x[1],RightPrecedenceGet(Type(x)));
  Decide(bracket,WriteString(")"));
}

10 ## Print(x_,n_)::(Postfix?(Type(x))&? ArgumentsCount(x) =? 1) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Decide(bracket,WriteString("("));
  Print(x[1],LeftPrecedenceGet(Type(x)));
  Write(x[0]);
  Decide(bracket,WriteString(")"));
}

20 ## Print(x_,n_)::(Type(x) =? "List") <--
{
  WriteString("[");
  PrintArg(x);
  WriteString("]");
}

20 ## Print(x_,n_)::(Type(x) =? "Sequence") <--
{
  WriteString("[");
  PrintArgBlock(Rest(ProcedureToList(x)));
  WriteString("]");
}
20 ## Print(x_,n_)::(Type(x) =? "Nth") <--
{
  Print(x[1],0);
  WriteString("[");
  Print(x[2],60000);
  WriteString("]");
}

100 ## Print(x_Procedure?,n_) <--
 {
   Write(x[0]);
   WriteString("(");
   PrintArg(Rest(ProcedureToList(x)));
   WriteString(")");
 }


/* Print the arguments of an ordinary procedure */
10 ## PrintArg([]) <-- True;

20 ## PrintArg(list_) <--
{
  Print(First(list),60000);
  PrintArgComma(Rest(list));
}
10 ## PrintArgComma([]) <-- True;
20 ## PrintArgComma(list_) <--
{
  WriteString(",");
  Print(First(list),60000);
  PrintArgComma(Rest(list));
}


18 ## Print(Complex(0,1),n_)   <-- {WriteString("I");}
19 ## Print(Complex(0,y_),n_)  <-- {WriteString("I*");Print(y,4);}
19 ## Print(Complex(x_,1),n_)  <-- {Print(x,7);WriteString("+I");}
20 ## Print(Complex(x_,y_),n_) <-- {Print(x,7);WriteString("+I*");Print(y,4);}


/* Tail-recursive printing the body of a compound statement */
10 ## PrintArgBlock([]) <-- True;
20 ## PrintArgBlock(list_) <--
{
   Print(First(list),60000);
   WriteString(";");
   PrintArgBlock(Rest(list));
}



%/mathpiper