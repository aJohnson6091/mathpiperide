%mathpiper,def="RemoveRepeated"

10 ## RemoveRepeated([]) <-- [];
10 ## RemoveRepeated([x_]) <-- [x];
20 ## RemoveRepeated(list_List?) <-- {
        Local(i, done);
        done := False;
        For(i:=0, !? done, i++)
        {
                While(i<?Length(list) &? list[i]=?list[i+1])
                        Delete!(list, i);
                Decide(i=?Length(list), done := True);
        }
        list;
}

%/mathpiper



%mathpiper_docs,name="RemoveRepeated",categories="Programming Procedures,Lists (Operations)"
*CMD RemoveRepeated --- Remove repeated entries within a list
*STD
*CALL
        RemoveRepeated(list)

*PARMS

{list} -- list from which to remove repeated entries

*DESC

Removes entries that are repeated more than once in a row. RemoveRepeated does not remove all duplicate entries, only entries which are repeated more than once in a row. For example,  RemoveRepeated([1,2,2,2,3,4,5,5,4]) will return [1,2,3,4,5,4]. The additional 2's and 5 were removed, however, both 4's remain because they were separated with two 5's.

*E.G.

In> RemoveRepeated([1,2,2,3,3,3,4,4,4,4]);
Result: [1,2,3,4];

In> RemoveRepeated(["a","a","b","bb","b","c","cc","c","c"]);
Result: ["a","b","bb","b","c","cc","c"]

In> RemoveRepeated([1,2,1,2,1,2])
Result: [1,2,1,2,1,2]

%/mathpiper_docs