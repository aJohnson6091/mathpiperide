%mathpiper,def="Difference"

Procedure("Difference",["list1", "list2"])
{
  Local(l2,index,result);
  l2:=FlatCopy(list2);
  result:=FlatCopy(list1);
  ForEach(item,list1)
  {
    Assign(index,Find(l2,item));
    Decide(index>?0,
      {
        Delete!(l2,index);
        Delete!(result,Find(result,item));
      }
      );
  }
  result;
}

%/mathpiper



%mathpiper_docs,name="Difference",categories="Programming Procedures,Lists (Operations)"
*CMD Difference --- return the difference of two lists
*STD
*CALL
        Difference(l1, l2)

*PARMS

{l1}, {l2} -- two lists

*DESC

The difference of the lists "l1" and "l2" is determined and
returned. The difference contains all elements that occur in "l1"
but not in "l2". The order of elements in "l1" is preserved. If a
certain expression occurs "n1" times in the first list and "n2"
times in the second list, it will occur "n1-n2" times in the result
if "n1" is greater than "n2" and not at all otherwise.

*E.G.

In> Difference([_a,_b,_c], [_b,_c,_d]);
Result: [_a];

In> Difference([_a,_e,_i,_o,_u],[_f,_o,_u,_r,_t,_e,_e,_n]);
Result: [_a,_i];

In> Difference([1,2,2,3,3,3], [2,2,3,4,4]);
Result: [1,3,3];

*SEE Intersection, Union
%/mathpiper_docs