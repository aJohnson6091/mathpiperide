%mathpiper,def="PrintList"

//////////////////////////////////////////////////
/// Print a list using a padding string
//////////////////////////////////////////////////

10 ## PrintList(list_List?) <-- PrintList(list, ", ");
10 ## PrintList([], padding_String?) <-- "";
20 ## PrintList(list_List?, padding_String?) <-- PipeToString() {
        Local(i);
        ForEach(i, list) {
                Decide(Not?(Equal?(i, First(list))), WriteString(padding));
                Decide(String?(i), WriteString(i), Decide(List?(i), WriteString("[" + PrintList(i, padding) + "]"), Write(i)));
        }
}

%/mathpiper



%mathpiper_docs,name="PrintList",categories="Programming Procedures,Lists (Operations)"
*CMD PrintList --- print list with padding
*STD
*CALL
        PrintList(list)
        PrintList(list, padding);

*PARMS

{list} -- a list to be printed

{padding} -- (optional) a string

*DESC

Prints {list} and inserts the {padding} string between each pair of items of the list. 
Items of the list which are strings  are printed without quotes, unlike {Write()}. 
Items of the list which are themselves lists are printed inside brackets {[]}. 
If padding is not specified, a standard one is used (comma, space).

*E.G.

In> PrintList([_a,_b,[_c,_d]], " .. ")
Result: " _a ..  _b .. [ _c ..  _d]";

*SEE Write, WriteString
%/mathpiper_docs