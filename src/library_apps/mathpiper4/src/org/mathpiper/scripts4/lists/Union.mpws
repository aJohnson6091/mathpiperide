%mathpiper,def="Union"

Procedure("Union",["list1", "list2"])
{
  RemoveDuplicates(Concat(list1,list2));
}

%/mathpiper



%mathpiper_docs,name="Union",categories="Programming Procedures,Lists (Operations)"
*CMD Union --- return the union of two lists
*STD
*CALL
        Union(l1, l2)

*PARMS

{l1}, {l2} -- two lists

*DESC

The union of the lists "l1" and "l2" is determined and
returned. The union contains all elements that occur in one or both of
the lists. In the resulting list, any element will occur only once.

*E.G.

In> Union([_a,_b,_c], [_b,_c,_d]);
Result: [_a,_b,_c,_d];

In> Union([_a,_e,_i,_o,_u], [_f,_o,_u,_r,_t,_e,_e,_n]);
Result: [_a,_e,_i,_o,_u,_f,_r,_t,_n];

In> Union([1,2,2,3,3,3], [2,2,3,3,4,4]);
Result: [1,2,3,4];

*SEE Intersection, Difference
%/mathpiper_docs