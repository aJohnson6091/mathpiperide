%mathpiper,def="FindAll"
  
10 ## FindAll( list_List?, element_ ) <--
  {
     Local(results,count);
     results := [];
     count   := 1;
     While( !? list =? [] )
     {
       Decide(Equal?( First(list), element), Append!(results,count) );
       list := Rest(list);
       count++;
     }
     results;
  }

%/mathpiper



%mathpiper_docs,name="FindAll",categories="Programming Procedures,Lists (Operations)"
*CMD Find --- get all indexes at which a certain element occurs
*STD
*CALL
        FindAll(list, expr)

*PARMS

{list} -- the list to examine

{expr} -- expression to look for in "list"

*DESC

This commands returns a list of all the indexes at which the 
expression "expr" occurs in "list". If "expr" occurs only once, 
ithat ndex is returned as a list. If "expr" does not occur at all,
the empty list, [], is returned.

*E.G.

In> FindAll([_a,_b,_c,_d,_e,_f], _d);
Result: [4]

In> FindAll([1,2,3,2,1], 2);
Result: [2,4]

In> FindAll([1,2,3,2,1], 4);
Result: []


*SEE Find, FindFirst, Contains?
%/mathpiper_docs
