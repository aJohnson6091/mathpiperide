%mathpiper,def="ContFrac;ContFracDoPoly"

//////////////////////////////////////////////////
/// continued fractions for polynomials
//////////////////////////////////////////////////

/// main interface
10 ## ContFrac(n_) <-- ContFrac(n, 6);
50 ## ContFrac(n_,depth_) <-- ContFracEval(ContFracList(n, depth), rest);

40 ## ContFrac(n_CanBeUni,depth_)::(Length(VarList(n)) =? 1) <--
{
  ContFracDoPoly(n,depth,VarList(n)[1]);
}

5  ## ContFracDoPoly(exp_,0,var_) <-- rest;
5  ## ContFracDoPoly(0,0,var_) <-- rest;
10 ## ContFracDoPoly(exp_,depth_,var_) <--
{
  Local(content,exp2,first,second);
  first:=Coef(exp,var,0);
  exp:=exp-first;
  content:=Content(exp);
  exp2:=DivPoly(1,PrimitivePart(exp),var,5+3*depth)-1;
  second:=Coef(exp2,0);
  exp2 := exp2 - second;
  first+content/((1+second)+ContFracDoPoly(exp2,depth-1,var));
}

%/mathpiper



%mathpiper_docs,name="ContFrac",categories="Mathematics Procedures,Numbers (Operations)"
*CMD ContFrac --- continued fraction expansion
*STD
*CALL
        ContFrac(x)
        ContFrac(x, depth)

*PARMS

{x} -- number or polynomial to expand in continued fractions

{depth} -- integer, maximum required depth of result

*DESC

This command returns the continued fraction expansion of {x}, which
should be either a floating point number or a polynomial. If
{depth} is not specified, it defaults to 6. The remainder is
denoted by {rest}.

This is especially useful for polynomials, since series expansions
that converge slowly will typically converge a lot faster if
calculated using a continued fraction expansion.

*E.G.

In> UnparseMath2D(ContFrac(NM(Pi)))
        
                     1
        --------------------------- + 3
                   1
        ----------------------- + 7
                1
        ------------------ + 15
              1
        -------------- + 1
           1
        -------- + 292
        rest + 1

Result: True;

In> UnparseMath2D(ContFrac(x^2+x+1, 3))
        
               x
        ---------------- + 1
                 x
        1 - ------------
               x
            -------- + 1
            rest + 1
        
Result: True;

*SEE NM
%/mathpiper_docs

*SEE PAdicExpand