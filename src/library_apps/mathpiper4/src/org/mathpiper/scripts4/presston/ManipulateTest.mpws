%mathpiper,def="ManipulateTest"

//Retract("ManipulateTest", All);

RulebaseElementaryAlgebra();

RulebaseListedHoldArguments("ManipulateTest",["exp", "unk", "optionsList"]);

//Handle no options call.
5 ## ManipulateTest(exp_, unk_) <-- ManipulateTest(exp, unk, []);


//Handle a single option call because the option does not come in a list for some reason.
20 ## ManipulateTest(exp_, unk_, singleOption_) <-- ManipulateTest(exp, unk, [singleOption]);



//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 ## ManipulateTest(exp_, unk_, optionsList_List?) <--
{
    Local(options, result, stepsCount, stepsIndex, step, stepsAll);

    options := OptionsToAssociationList(optionsList);
    
    If(options["StepsAll"] !=? None)
    {
        stepsAll := options["StepsAll"];
    }
    Else
    {
        stepsAll := SolveSteps(exp, unk);
    }
    
    result := True;
    
    stepsCount := Length(stepsAll);
    
    stepsIndex := 2;
    
    While(stepsIndex <=? stepsCount)
    {
        If(stepsIndex =? 2)
        {
            step := SolveSteps(exp, unk, MaximumSteps:1)[2];
        }
        Else
        {
            step := SolveSteps(exp, unk, MaximumSteps:1, DisableOthersideRHS:True)[2];
        }
    
        If(step =? stepsAll[stepsIndex])
        {
            If(options["Trace"] =? True) Echo("Step: " + (stepsIndex - 1) + ", " + step["RuleName"]);
        }
        Else
        {
            result := False;
            
            If(options["Trace"] =? True) 
            {
                Echo("Nomatch: " + (stepsIndex - 1) + ", " + step["RuleName"] + ", StepsAll -> " + stepsAll[stepsIndex]["RuleName"]);
                Echo(exp);
                Echo(step["ReplacementExp"][1]);
                Echo("Position: ", step["Position"]);
            }
            
            ?step := step;
            ?stepsAll := stepsAll;
            ?stepsAllCurrentIndex := stepsAll[stepsIndex];
            ?expression := exp;
            ?unknown := unk;

            Break();
        }
        
        exp := step["ReplacementExp"][1];
        
        If(options["ListDepths?"] =? True)
        {
            Write(TreeDepth(exp));
            WriteString(",");
        }
        
        stepsIndex++;
    }
    
    result;
}

%/mathpiper




%mathpiper,title="Manual test."

//ManipulateTest('(8*_x - 2 == -9 + 7*_x), _x, Trace:True);
//ManipulateTest('(- (7-(4*_x)) == 9), _x, Trace:True);
//ManipulateTest('(6 == (1-(2*_n))+5), _n, Trace:True);
//ManipulateTest('( ( - 5)*(1-(5*_x))+5*(( - 8)*_x-2) == ( - 4)*_x-(8*_x) ), _x, Trace:True);
//ManipulateTest('(1/2*(4*_x - 6) == 1/2*(2*(_x - 3))), _x, Trace:True); // todo:tk:1/2 evaluates to 1/2.
//ManipulateTest(-20 == ( -4)*_x - (6*_x), _x, Trace:True);
//ManipulateTest('(-20 == ( -4*_x)*(6*_x)), _x, Trace:True);
//ManipulateTest('( ( _x )/( 2*_x + 7) == 4  ), _x, Trace:True); //todo:tk:fail. a new rule is needed to change to 1/(2+((1/_x)*7))==4
//ManipulateTest('(_x - 3.4 == ( _x - 2.5)/( 1.3)), _x, Trace:True);
//ManipulateTest( MathParse("2x/3 = 2x - 4"), _x, Trace:True );

ManipulateTest(MathParse("( x )/( 2x + 7) = 4"), _x, Trace:True);

%/mathpiper
