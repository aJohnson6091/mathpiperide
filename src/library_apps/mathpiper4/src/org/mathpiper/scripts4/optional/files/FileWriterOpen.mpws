%mathpiper,def="FileWriterOpen"

FileWriterOpen(fileName) :=
{
    Check(String?(fileName), "The filename argument must be a string.");

    Local(fileWriter);
    
    fileWriter := JavaNew("java.io.FileWriter", fileName);
    
    JavaNew("java.io.BufferedWriter", fileWriter);
}



FileWriterOpen(fileName, append?) :=
{
    Check(String?(fileName), "The filename argument must be a string.");

    Check(Boolean?(append?), "The append argument must be True or False.");

    Local(fileWriter);
    
    fileWriter := JavaNew("java.io.FileWriter", fileName, JavaNew("java.lang.Boolean", append?));
    
    JavaNew("java.io.BufferedWriter", fileWriter);
}

%/mathpiper





%mathpiper_docs,name="FileWriterOpen",categories="Programming Procedures,Input/Output",access="experimental"
*CMD FileWriterOpen - opens a file
*CALL
        FileWriterOpen(filename)
        FileWriterOpen(filename, append?)

*PARMS
filename --- the full path of a file in a string
append? --- "True" means append to the file if it exists, and "False" means replace the file if it exists.

*DESC
This procedure opens a file for writing. The full path of the file must be given.

*E.G.
In> FileWriterOpen("<filename>")
%/mathpiper_docs









