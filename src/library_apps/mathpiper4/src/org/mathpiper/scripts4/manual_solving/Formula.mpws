%mathpiper,def="Formula;FormulaHelper;UnitsStrip"

UnitsStrip(expression) :=
{
    expression /:: [(a_ ~ b_)/*::Type(a) !=? "ArcTan" &? b !=? 'rad*/ <- a];
}


Retract("Formula", All);
Retract("FormulaHelper", All);

RulebaseListedHoldArguments("Formula", ["formula"]);
RulebaseListedHoldArguments("Formula", ["formula", "optionsList"]);
HoldArgument("Formula", "formula");
HoldArgument("Formula", "optionsList");



/*

//Handle no options call.
5 ## Formula(formula_) <-- `Formula(@formula[1], []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
Formula(formula_, optionsList_List?) <--
{
}


//Handle a single option call because the option does not come in a list for some reason.
20 ## Formula(formula_, singleOption_) <-- `Formula(@formula, [singleOption]);
*/


//=======================

RulebaseListedHoldArguments("FormulaHelper", ["formula"]);
RulebaseListedHoldArguments("FormulaHelper", ["formula", "optionsList"]);
HoldArgument("FormulaHelper", "formula");


//Handle no options call.
5 ## FormulaHelper(formula_) <-- `FormulaHelper(@formula, []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 ## FormulaHelper(formula_, optionsList_List?) <--
{
    If(Verbose?())
    {
        Echo("Formula");
    }
   
    Local(options, definedOptionKeys, optionKeys, substitutions, subject, strippedFormula);
    
    definedOptionKeys := ["Subject", "Description", "Substitutions", "Label", "Page"];

    options := OptionsToAssociationList(optionsList);
   
    ForEach(option, options)
    {
        If(!? Contains?(definedOptionKeys, option[1]))
        {
            ExceptionThrow("", "The option name \"" + option[1] + "\" is not defined.");
        }
    }
    
    options["OriginalFormula"] := formula;
    options["Transformed?"] := False;

    subject := Decide(Atom?(options["Subject"]), options["Subject"], None);
    substitutions := Decide(List?(options["Substitutions"]), options["Substitutions"], None);
  
    strippedFormula := UnitsStrip(formula);
  
    If(substitutions !=? None)
    {
        strippedFormula := Substitute2(strippedFormula, substitutions);
        options["Transformed?"] := True;
    }

    If(subject !=? None)
    {
        If(Type(strippedFormula) =? "==")
        {
            options["FinalFormula"] := Rearrange(strippedFormula, subject);
        }
        Else
        {
            options["FinalFormula"] := Rearrange(Eval(strippedFormula), subject);
        }
        options["Transformed?"] := True;
    }
    Else
    {
        options["FinalFormula"] := strippedFormula;
    }
    
    options;
}


//Handle a single option call because the option does not come in a list for some reason.
20 ## FormulaHelper(formula_, singleOption_) <-- `FormulaHelper(@formula, [singleOption]);

%/mathpiper

    %output,mpversion=".279",preserve="false"
      Result: True
.   %/output







%mathpiper_docs,name="Formula",categories="Programming Procedures,Miscellaneous"
*CMD Formula --- define a formula in a ProblemSolution body

*CALL
        Formula(formula, <options>)

*PARMS

{formula} -- a formula


*DESC

Evaluate an expression inside of a {Formulas} body.

%/mathpiper_docs
