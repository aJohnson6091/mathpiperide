%mathpiper,def="Monomial?;CanBeMonomial"

10 ## CanBeMonomial(expr_)::(Type(expr)=?"UniVariate") <-- False;

10 ## CanBeMonomial(expr_)<--!? (HasProcedure?(expr,ToAtom("+")) |? HasProcedure?(expr,ToAtom("-")));

10 ## Monomial?(expr_CanBeMonomial) <-- 
{
    Local(r);
    Decide( RationalFunction?(expr),
        r := (VarList(Denominator(expr)) =? []),
        r := True
    );
}

15 ## Monomial?(expr_) <-- False;

%/mathpiper








%mathpiper_docs,name="Monomial?",categories="Programming Procedures,Predicates"
*CMD Monomial? --- determine if [expr] is a Monomial
*STD
*CALL
        Monomial?(expr)

*PARMS
{expr} -- an expression

*DESC
This procedure returns {True} if {expr} satisfies the definition of a {Monomial}.
Otherwise, {False}.
A {Monomial} is defined to be a single term, consisting of a product of numbers
and variables.

*E.G.

In> Monomial?(24)
Result: True


In> Monomial?(24*a*x^2*y^3)
Result: True


In> Monomial?(24*a*x^2*y^3/15)
Result: True


In> Monomial?(24*a*x^2*y^3/15+1)
Result: False
    
%/mathpiper_docs
