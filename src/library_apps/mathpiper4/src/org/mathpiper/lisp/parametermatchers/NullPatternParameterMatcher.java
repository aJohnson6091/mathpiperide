/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.lisp.parametermatchers;

import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;

/**
 * Always match, used to stop the recursive matching process. Checks if at the end
 */
public class NullPatternParameterMatcher extends PatternParameterMatcher {
    
    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons aExpressionTree, Cons[] arguments) throws Throwable {
        return aExpressionTree == null;
    }
    
    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons[] aExpressionTree, int aTreeLocation, Cons[] arguments) throws Throwable {
        return aExpressionTree == null || aTreeLocation == aExpressionTree.length;
    }
    
    @Override
    public PatternParameterMatcher getNextMatcher() {
        return null;
    }
    
    @Override
    public String getType() {
        return "Null";
    }
    
    @Override
    public String toString() {
        return "_";
    }
}
