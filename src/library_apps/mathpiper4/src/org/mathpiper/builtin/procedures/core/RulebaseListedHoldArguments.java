/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class RulebaseListedHoldArguments extends BuiltinProcedure
{

    private RulebaseListedHoldArguments()
    {
    }

    public RulebaseListedHoldArguments(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        org.mathpiper.lisp.Utility.defineRulebase(aEnvironment, aStackTop, true);
    }
}



/*
%mathpiper_docs,name="RulebaseListedHoldArguments",categories="Programming Procedures,Miscellaneous,Built In"
*CMD RulebaseListedHoldArguments --- define procedure with variable number of arguments
*CORE
*CALL
	RulebaseListedHoldArguments("name", params)

*PARMS

{"name"} -- string, name of procedure

{params} -- list of arguments to procedure

*DESC

The command {RulebaseListedHoldArguments} defines a new procedure. It essentially works the
same way as {RulebaseHoldArguments}, except that it declares a new procedure with a variable
number of arguments. The list of parameters {params} determines the smallest
number of arguments that the new procedure will accept. If the number of
arguments passed to the new procedure is larger than the number of parameters in
{params}, then the last argument actually passed to the new procedure will be a
list containing all the remaining arguments.

A procedure defined using {RulebaseListedHoldArguments} will appear to have the arity equal
to the number of parameters in the {param} list, and it can accept any number
of arguments greater or equal than that. As a consequence, it will be impossible to define a 
new procedure with the same name and with a greater arity.

The procedure body will know that the procedure is passed more arguments than the
length of the {param} list, because the last argument will then be a list. The
rest then works like a {Rulebase}-defined procedure with a fixed number of
arguments. Transformation rules can be defined for the new procedure as usual.


*E.G.

The definitions

    RulebaseListedHoldArguments("f",["a", "b", "c"]);
    10 ## f(_a,_b,[_c,_d]) <-- Echo(["four args",a,b,c,d]);
    20 ## f(_a,_b,c_List?) <-- Echo(["more than four args",a,b,c]);
    30 ## f(_a,_b,_c) <-- Echo(["three args",a,b,c]);
	
give the following interaction:
In> Function() f(_a); Function() f(_a,_b);

In> f(_A) 
Result: f(_A);

In> f(_A,_B) 
Result: f(_A,_B);

In> f(_A,_B,_C)
Result: True;
Side Effects:
three args _A _B _C

In> f(_A,_B,_C,_D)
Result: True
Side Effects:
four args _A _B _C _D

In> f(_A,_B,_C,_D,_E)
Result: True
Side Effects:
more than four args _A _B [_C,_D,_E]

In> f(_A,_B,_C,_D,_E,_E)
Result: True
Side Effects:
more than four args _A _B [_C,_D,_E,_E]

The procedure {f} now appears to occupy all arities greater than 3:

In> RulebaseHoldArguments("f", {x,y,z,t});
	CommandLine(1) : Rule base with this arity already defined


*SEE RulebaseHoldArguments, Retract, Echo
%/mathpiper_docs
*/
