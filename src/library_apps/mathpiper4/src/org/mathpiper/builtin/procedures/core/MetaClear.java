/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;


import java.util.HashMap;
import java.util.Map;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;



public class MetaClear extends BuiltinProcedure
{

    private MetaClear()
    {
    }

    public MetaClear(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons object = getArgument(aEnvironment, aStackTop, 1);

        object.setMetadataMap(null);


        setTopOfStack(aEnvironment, aStackTop, object);

        return;

    }//end method.


}//end class.


/*
%mathpiper_docs,name="MetaClear",categories="Programming Procedures,Built In"
 *CMD MetaClear --- set the metadata for a value or an unbound variable
 *CORE
 *CALL
MetaSet(value_or_unbound_variable)

 *PARMS

{value_or_unbound_variable} -- a value or an unbound variable


 *DESC

Clears a value's metadata.  MetaClear returns the given value or unbound variable
as a result after it has had metadata added to it.

*E.G.
In> MetaClear(_b)
Result: _b


*SEE MetaGet, MetaSet, MetaKeys, MetaValues, Unassign
%/mathpiper_docs
 */
