/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.SynchronousInterpreter;
import org.mathpiper.io.StringInputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.lisp.parsers.MathPiperParser;
import org.mathpiper.lisp.parsers.Parser;
import org.mathpiper.lisp.parsers.LispParser;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;
import org.mathpiper.ui.gui.RulesPanel;
import org.mathpiper.ui.gui.TreeFeaturesPanel;
import org.mathpiper.ui.gui.worksheets.LatexRenderingController;
import org.mathpiper.ui.gui.worksheets.MathPanelController;
import org.mathpiper.ui.gui.worksheets.ScreenCapturePanel;
import org.mathpiper.ui.gui.worksheets.TreePanelCons;
import org.scilab.forge.mp.jlatexmath.TeXFormula;

/**
 *
 *
 */
public class TreeView extends BuiltinProcedure implements ActionListener {
    // Show(TreeView( "a*(b+c) == a*b + a*c",  Resizable: True, IncludeExpression: True))
    // Show(TreeView( "(+ 1 2)", Prefix: True, Code: True, Resizable: True, IncludeExpression: True))
    // MetaSet(Car(Cdr(Car('(a+(b+c) == d)))),"op",True)
    // Show(StepsView(SolveEquation( '( ((- a^2) * b )/ c + d == e ), a), ShowTree: True))

    private Map defaultOptions;
    
    private List<ResponseListener> responseListeners = new ArrayList<ResponseListener>();

    private TreePanelCons treePanel;
    
    private JTabbedPane tabbedPane;

    private RulesPanel rulesPanel;

    private Cons candidateResult;

    private String candidateRuleName;

    private String candidatePositionString;

    private List<String> steps;

    private List<Cons> expressionSteps;

    private int expressionStepsIndex = 0;

    private TeXFormula formula;

    private JLabel latexLabel;

    private LatexRenderingController latexPanelController;

    private JButton acceptButton;
    
    private JButton applyButton;
    
    private JCheckBox showPositionsCheckBox = new JCheckBox("Show Positions");
    
    private JCheckBox showDepthsCheckBox = new JCheckBox("Show Depths");
    
    private Environment environment;
    
    private JButton nextStepButton;
    
    private String theUnknown;
    
    private TreeFeaturesPanel treeFeaturesPanel;
    
    private JCheckBox subtreeCheckBox;
    
    private JCheckBox ruleCheckBox;
    
    private JButton undoStepButton;
    
    private JButton redoStepButton;
    
    private JSplitPane horizontalSplitPane;
    
    private Integer horizontalDividerLocation = -1;
    
    private boolean isDisableOthersideRHS = false;

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "TreeView";

        aEnvironment.getBuiltinFunctions().put("TreeView", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();

        defaultOptions.put("Scale", 2.5);
        defaultOptions.put("FontSize", 40.0);
        defaultOptions.put("Resizable", true);
        defaultOptions.put("IncludeExpression", true);
        defaultOptions.put("Lisp", false);
        defaultOptions.put("Code", true);
        defaultOptions.put("Debug", false);
        defaultOptions.put("WordWrap", 0);
        defaultOptions.put("ShowPositions", false);
        defaultOptions.put("ShowDepths", false);
        defaultOptions.put("PathHighlight", null);
        defaultOptions.put("Manipulate", false);
        defaultOptions.put("ArcsHighlight", false);
        defaultOptions.put("NodesHighlight", false);
        defaultOptions.put("PathNumbers", false);
        defaultOptions.put("ArcsAutoHighlight", true);
        defaultOptions.put("TabTitle", "");
        defaultOptions.put("TabDescription", "");
        defaultOptions.put("Hints", false);
        defaultOptions.put("DividerLocation", -1);
        defaultOptions.put("TheUnknown", null);
        defaultOptions.put("FullParentheses", false);
        defaultOptions.put("NoParentheses?", false);
        defaultOptions.put("Selectable", false);
        defaultOptions.put("ReturnList", false);
        

    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        
        this.environment = aEnvironment;
        
        tabbedPane = null;
        
        rulesPanel = null;
        
        isDisableOthersideRHS = false;
        
        String startPositionString = null;
        
        String endPositionString = null;
        

        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        Cons options = (Cons) Cons.cddar(arguments);

        final Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        
        boolean includeSlider = (Boolean) userOptions.get("Resizable");
        int fontSize = ((Double) userOptions.get("FontSize")).intValue();
        boolean includeExpression = (Boolean) userOptions.get("IncludeExpression");
        Cons pathHighlight =  (Cons) userOptions.get("PathHighlight");
        boolean arcsHighlight = (Boolean) userOptions.get("ArcsHighlight");
        boolean nodesHighlight = (Boolean) userOptions.get("NodesHighlight");
        boolean manipulate = (Boolean) userOptions.get("Manipulate");
        String tabTitle = (String) userOptions.get("TabTitle");
        String tabDescription = (String) userOptions.get("TabDescription");
        boolean returnList = (Boolean) userOptions.get("ReturnList");
        Object object = userOptions.get("DividerLocation");
        if(object instanceof Integer)
        {
            horizontalDividerLocation = (Integer) object;
        }
        else if(object instanceof Double)
        {
            horizontalDividerLocation = ((Double) object).intValue();
        }
        theUnknown = (String) userOptions.get("TheUnknown");
        if (theUnknown != null && !theUnknown.startsWith("_")) 
        {
            theUnknown = "_" + theUnknown;
        }
        
        boolean fullParentheses = (Boolean) userOptions.get("FullParentheses");
        boolean isNoParentheses = (Boolean) userOptions.get("NoParentheses?");
        
        
        if(pathHighlight != null)
        {
            pathHighlight = (Cons) pathHighlight.car();

            int listLength = Utility.listLength(aEnvironment, aStackTop, pathHighlight) ;
            if(listLength == 3)
            {
                pathHighlight = pathHighlight.cdr();
                Object startPosition = pathHighlight.car();

                pathHighlight = pathHighlight.cdr();
                Object endPosition = pathHighlight.car();

                if(startPosition instanceof String && endPosition instanceof String)
                {
                    startPositionString = Utility.stripEndQuotesIfPresent((String) startPosition);
                    endPositionString = Utility.stripEndQuotesIfPresent((String) endPosition);
                }
                else
                {
                    LispError.throwError(aEnvironment, aStackTop, "Both elements of the PathHighlight option must be strings");
                }
                
            }
        }

        Object argument = ((Cons) getArgument(aEnvironment, aStackTop, 1).car()).cdr().car();

        String texString = "";

        Cons expression = null;

        if ((argument instanceof String)) {

            String expressionString = (String) argument;

            expressionString = Utility.stripEndQuotesIfPresent(expressionString);

            Parser parser;

            if (((Boolean) userOptions.get("Lisp")) == true) {
                texString = expressionString;
                texString = texString.replace(" ", "\\ ");

                StringInputStream newInput = new StringInputStream(expressionString, aEnvironment.iInputStatus);

                parser = new LispParser(aEnvironment.iCurrentTokenizer, newInput, aEnvironment);
            } else {
                texString = expressionString;

                if (!expressionString.endsWith(";")) {
                    expressionString = expressionString + ";";
                }
                expressionString = expressionString.replaceAll(";;;", ";");
                expressionString = expressionString.replaceAll(";;", ";");

                StringInputStream newInput = new StringInputStream(expressionString, aEnvironment.iInputStatus);

                parser = new MathPiperParser(new MathPiperTokenizer(), newInput, aEnvironment, aEnvironment.iPrefixOperators, aEnvironment.iInfixOperators, aEnvironment.iPostfixOperators, aEnvironment.iBodiedProcedures);
            }

            expression = parser.parse(aStackTop);

        } else {

            if (!Utility.isSublist(arguments)) {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }

            arguments = (Cons) arguments.car(); //Go to sub list.

            arguments = arguments.cdr(); //Strip List tag.

            expression = (Cons) arguments;
            expression.setCdr(null);
            //expression = (Cons) argument;

        }

        Box latexBox = new Box(BoxLayout.Y_AXIS);

        // Box treeBox = new Box(BoxLayout.Y_AXIS);
        JPanel treeBox = new JPanel(new BorderLayout());

        JPanel latexScreenCapturePanel = new ScreenCapturePanel();
        if (!((Boolean) userOptions.get("Lisp"))) {
            if (!((Boolean) userOptions.get("Code"))) {
                try {
                    String latexString = expressionToLatex(aEnvironment, aStackTop, expression);

                    formula = new TeXFormula(latexString);
                    latexLabel = new JLabel();
                    latexPanelController = new LatexRenderingController(formula, latexLabel, fontSize); // Do not delete.
                    latexPanelController.setLayout(new FlowLayout(FlowLayout.LEFT));
                    latexScreenCapturePanel.add(latexLabel);
                    latexScreenCapturePanel.setLayout(new FlowLayout(FlowLayout.LEFT));

                    JScrollPane latexScrollPane = new JScrollPane(latexScreenCapturePanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

                    latexBox.add(latexScrollPane);
                    
                    if(includeSlider)
                    {
                        latexBox.add(latexPanelController);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                    JOptionPane.showMessageDialog(null, "There has been an internal error (1).", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                texString = Utility.printMathPiperExpression(aStackTop, expression, aEnvironment, -1, false, fullParentheses, isNoParentheses);
                JLabel codeLabel = new JLabel(texString);
                codeLabel.setFont(new Font("Serif", Font.PLAIN, fontSize));
                latexScreenCapturePanel.add(codeLabel);
                latexScreenCapturePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
                JScrollPane latexScrollPane = new JScrollPane(latexScreenCapturePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

                latexBox.add(latexScrollPane);
            }
        }

        double viewScale = ((Double) userOptions.get("Scale")).doubleValue();

        JPanel panel = new TreeViewPanel();
        panel.setLayout(new BorderLayout());

        panel.setBackground(Color.white);
	//box.setOpaque(true);

        this.steps = new ArrayList<String>();

        this.expressionSteps = new ArrayList<Cons>();

        if(manipulate)
        {
            this.saveStep(aEnvironment, aStackTop, expression, "", "");
        }

        Cons expressionCopy = Cons.deepCopy(aEnvironment, aStackTop, expression);
        expressionSteps.add(expressionCopy);
        expressionStepsIndex = 0;

        treePanel = new TreePanelCons(aEnvironment, expression, viewScale, userOptions);
        treePanel.setIsSelectSingleNode(true);

        final Environment environment = aEnvironment;
        final int stackTop = aStackTop;

        treePanel.addResponseListener(new ResponseListener() {

            public void response(EvaluationResponse response) {
                
                if(response.getSideEffects().equals("NotCleared"))
                {
                    return;
                }
                
                showPositionsCheckBox.setSelected(false);
                    
                showDepthsCheckBox.setSelected(false);
                    
                String positionString = response.getResult();
                Cons expression = response.getResultList();
                
                // Highlight or unhighlight the linear view of the expression.
                try {
                    TreeView.this.clearMetaInformation(expression);

                    if (positionString != null) {
                        Cons subExpression = expressionGet(environment, stackTop, expression, positionString);

                        highlightTree(environment, stackTop, subExpression, "YellowOrange");
                        
                        Cons dominantNode;
                        
                        if(subExpression instanceof SublistCons)
                        {
                            dominantNode = (Cons) subExpression.car();
                        }
                        else
                        {
                            dominantNode = subExpression;
                        }
                        
                        highlightNode(environment, stackTop, dominantNode, "ForestGreen");
                    }
                    
                    

                    if(TreeView.this.acceptButton != null)
                    {
                        TreeView.this.acceptButton.setEnabled(false);
                    }
                    
                    if(formula != null)
                    {
                        String latexString = expressionToLatex(environment, stackTop, expression);
                        formula.setLaTeX(latexString);
                        TreeView.this.latexPanelController.adjust();
                    }
                    
                    
                    if(TreeView.this.rulesPanel != null)
                    {
                        if(response.getSideEffects().equals("Selected"))
                        {
                            if(TreeView.this.rulesPanel.getSelectedRowCount() != 0)
                            {
                               TreeView.this.applyButton.setEnabled(true); 
                            }
                        }
                        else
                        {
                            TreeView.this.applyButton.setEnabled(false);
                        }
                    }
                    
                } catch (Throwable t) {
                    t.printStackTrace();
                    JOptionPane.showMessageDialog(null, "There has been an internal error (2).", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

            public boolean remove() {
                return false;
            }
        });

        JPanel treeScreenCapturePanel = new ScreenCapturePanel();

        treeScreenCapturePanel.add(treePanel);

        //JPanel screenCapturePanel = new ScreenCapturePanel();   
        //screenCapturePanel.add(treePanel);




        if (manipulate) {
            
            JPanel buttonPanel = new JPanel();
            
            buttonPanel.setBorder(BorderFactory.createTitledBorder("Steps"));

            tabbedPane = new JTabbedPane();
            
            tabbedPane.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    
                    showPositionsCheckBox.setSelected(false);
                    
                    showDepthsCheckBox.setSelected(false);

                    if(tabbedPane.getSelectedIndex() == 0)
                    {
                        treePanel.setIsSelectSingleNode(false);
                        //showPositionsCheckBox.setEnabled(true);
                    }
                    else
                    {
                        treePanel.setIsSelectSingleNode(true);
                        //showPositionsCheckBox.setEnabled(false);
                    }
                    
                    
                    try
                    {
                        Cons expression = treePanel.getExpression(environment, stackTop);
                        
                        TreeView.this.clearMetaInformation(expression);
                        
                        treePanel.redrawTree(expression);
                        
                        if(formula != null)
                        {
                            String latexString = expressionToLatex(environment, stackTop, expression);
                            formula.setLaTeX(latexString);
                            TreeView.this.latexPanelController.adjust();
                        }
                    }
                    catch (Throwable t)
                    {
                        t.printStackTrace();
                        JOptionPane.showMessageDialog(null, "There has been an internal error (3).", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            
            rulesPanel = new RulesPanel(aEnvironment, userOptions);
            
            rulesPanel.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    if(TreeView.this.rulesPanel.getSelectedRowCount() != 0 && TreeView.this.treePanel.isSubtreeSelected())
                    {
                        TreeView.this.applyButton.setEnabled(true);
                    }
                }
            });
            
            tabbedPane.addTab(tabTitle, null, rulesPanel, tabDescription);
            
            treeFeaturesPanel = new TreeFeaturesPanel();
            
            treeFeaturesPanel.addActionListener(this);
            
            tabbedPane.addTab("Tree", null, treeFeaturesPanel, "Tree features.");
            
            this.rulesPanel.addButtonRow1(buttonPanel, -1);
        
        
            
            //panel.add(new RulesPanel(aEnvironment, userOptions), BorderLayout.EAST);

            applyButton = new JButton("Apply");
            
            applyButton.setEnabled(false);

            applyButton.addActionListener(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    if (treePanel.getPositionString() != null) {
                        String positionString = treePanel.getPositionString();
                        String operatorString = treePanel.getOperatorString();

                        if (rulesPanel.getSelectedRowCount() != 0) {
                            try {
                                int selectedRow = rulesPanel.getSelectedRow() + 1;

                                Cons cons = (Cons) userOptions.get("RewriteRulesTable");

                                if(cons == null)
                                {
                                    throw new Exception("No rewrite rules table was submitted.");
                                }
                                
                                Cons cons2 = Utility.nth(environment, -1, cons, selectedRow, true);

                                Cons ruleName = Utility.nth(environment, -1, cons2, 1, true);
                                String ruleNameString = Utility.toNormalString(environment, -1, ruleName.toString());
                                Cons pattern = Utility.nth(environment, -1, cons2, 2, true);
                                Cons patternTex = Utility.nth(environment, -1, cons2, 3, true);
                                String patternTexString = Utility.toNormalString(environment, -1, patternTex.toString());
                                Cons replacement = Utility.nth(environment, -1, cons2, 4, true);
                                replacement = ((Cons)replacement.car()).cdr();
                                Cons replacementTex = Utility.nth(environment, -1, cons2, 5, true);
                                String replacementTexString = Utility.toNormalString(environment, -1, replacementTex.toString());

                                Cons originalExpression = null;
                                Cons newExpression = null;


                                if (userOptions.containsKey("Substitute")) {
                                    Cons cons3 = (Cons) userOptions.get("Substitute");

                                    try {
                                        Cons from = AtomCons.getInstance(environment.getPrecision(), "\"expression\"");
                                        Cons to = TreeView.this.treePanel.getExpression(environment, -1);
                                        org.mathpiper.lisp.astprocessors.ExpressionSubstitute behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
                                        cons3 = Utility.substitute(environment, -1, cons3, behaviour);

                                        from = AtomCons.getInstance(environment.getPrecision(), "\"pattern\"");
                                        to = pattern;
                                        behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
                                        cons3 = Utility.substitute(environment, -1, cons3, behaviour);

                                        from = AtomCons.getInstance(environment.getPrecision(), "\"replacement\"");
                                        to = replacement;
                                        behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
                                        cons3 = Utility.substitute(environment, -1, cons3, behaviour);

                                        from = AtomCons.getInstance(environment.getPrecision(), "\"position\"");
                                        to = AtomCons.getInstance(environment.getPrecision(), "\"" + positionString + "\"");
                                        behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
                                        cons3 = Utility.substitute(environment, -1, cons3, behaviour);
                                    } catch (Throwable t) {
                                        t.printStackTrace();
                                        JOptionPane.showMessageDialog(null, "There has been an internal error (4).", "Error", JOptionPane.ERROR_MESSAGE);
                                    }

                                    Interpreter interpreter = SynchronousInterpreter.getInstance();

                                    EvaluationResponse response2 = interpreter.evaluate(cons3);

                                    if (response2.isExceptionThrown()) {
                                        JOptionPane.showMessageDialog(null, "There has been an internal error (5).", "Error", JOptionPane.ERROR_MESSAGE);
                                        System.err.println(response2.getException().getMessage());
                                        return;                                 
                                    }

                                    originalExpression = ((Cons) cons3.car()).cdr();
                                    originalExpression.setCdr(null);
                                    newExpression = response2.getResultList();
                                }

                                treePanel.redrawTree(newExpression);

                                TreeView.this.candidateResult = newExpression;

                                TreeView.this.candidateRuleName = ruleNameString;

                                TreeView.this.candidatePositionString = positionString;

                                boolean equal = Utility.equals(environment, stackTop, originalExpression, candidateResult);

                                if (equal) {
                                    acceptButton.setEnabled(false);

                                    clearMetaInformation(TreeView.this.candidateResult);

                                    if(formula != null)
                                    {
                                        String latexString = expressionToLatex(environment, stackTop, candidateResult);
                                        formula.setLaTeX(latexString);
                                        TreeView.this.latexPanelController.adjust();
                                    }

                                } else {
                                    acceptButton.setEnabled(true);

                                    try {
                                        Cons subExpression = candidateResult;
                                        
                                        subExpression = expressionGet(environment, stackTop, subExpression, positionString);

                                        highlightTree(environment, stackTop, subExpression, "ForestGreen");

                                        if(formula != null)
                                        {
                                            String latexString = expressionToLatex(environment, stackTop, candidateResult);
                                            formula.setLaTeX(latexString);
                                            TreeView.this.latexPanelController.adjust();
                                        }

                                        clearMetaInformation(TreeView.this.candidateResult);
                                    } catch (Throwable t) {
                                        t.printStackTrace();
                                        JOptionPane.showMessageDialog(null, "There has been an internal error (6).", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                }

                            } catch (Throwable t) {
                                t.printStackTrace();
                                JOptionPane.showMessageDialog(null, "There has been an internal error (7).", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                    
                    TreeView.this.applyButton.setEnabled(false);
                }
            });

            buttonPanel.add(applyButton);

            acceptButton = new JButton("Accept");
            acceptButton.setEnabled(false);
            acceptButton.addActionListener(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    TreeView.this.treePanel.setExpression(candidateResult);
                    treePanel.redrawTree(candidateResult);

                    try {
                        if(formula != null)
                        {
                            String latexString = expressionToLatex(environment, stackTop, candidateResult);
                            formula.setLaTeX(latexString);
                            TreeView.this.latexPanelController.adjust();
                        }

                        saveStep(environment, stackTop, candidateResult, candidateRuleName, candidatePositionString);

                        Cons expressionCopy = Cons.deepCopy(environment, stackTop, candidateResult);
                        expressionSteps.add(expressionCopy);
                        expressionStepsIndex = expressionSteps.size() - 1;
                    } catch (Throwable t) {
                        t.printStackTrace();
                        JOptionPane.showMessageDialog(null, "There has been an internal error (8).", "Error", JOptionPane.ERROR_MESSAGE);
                    }

                    acceptButton.setEnabled(false);
                    
                    TreeView.this.rulesPanel.clearSelection();
                    
                    TreeView.this.undoStepButton.setEnabled(true);
                    TreeView.this.redoStepButton.setEnabled(true);
                    
                }
            });
            buttonPanel.add(acceptButton);

            buttonPanel.add(Box.createGlue());


            undoStepButton = new JButton("Undo");
            
            undoStepButton.setEnabled(false);
            
            undoStepButton.addActionListener(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {

                    if (expressionStepsIndex - 1 != -1) {
                        expressionStepsIndex--;
                    }

                    Cons lastExpression = TreeView.this.expressionSteps.get(expressionStepsIndex);

                    TreeView.this.treePanel.setExpression(lastExpression);
                    treePanel.redrawTree(lastExpression);

                    try {
                        if(formula != null)
                        {
                            String latexString = expressionToLatex(environment, stackTop, lastExpression);
                            formula.setLaTeX(latexString);
                            TreeView.this.latexPanelController.adjust();
                        }

                        saveStep(environment, stackTop, lastExpression, "Undo Step", " ");
                    } catch (Throwable t) {
                        t.printStackTrace();
                        JOptionPane.showMessageDialog(null, "There has been an internal error (9).", "Error", JOptionPane.ERROR_MESSAGE);
                    }

                }
            });
            buttonPanel.add(undoStepButton);

            redoStepButton = new JButton("Redo");
            redoStepButton.setEnabled(false);
            redoStepButton.addActionListener(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {

                    if (expressionStepsIndex + 1 != expressionSteps.size()) {
                        expressionStepsIndex++;
                    }

                    Cons lastExpression = TreeView.this.expressionSteps.get(expressionStepsIndex);

                    TreeView.this.treePanel.setExpression(lastExpression);
                    treePanel.redrawTree(lastExpression);

                    try {
                        if(formula != null)
                        {
                            String latexString = expressionToLatex(environment, stackTop, lastExpression);
                            formula.setLaTeX(latexString);
                            TreeView.this.latexPanelController.adjust();
                        }

                        saveStep(environment, stackTop, lastExpression, "Redo Step", " ");
                    } catch (Throwable t) {
                        t.printStackTrace();
                        JOptionPane.showMessageDialog(null, "There has been an internal error (10).", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            buttonPanel.add(redoStepButton);
    
            
            JButton showStepsButton = new JButton("Show");
            showStepsButton.addActionListener(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    final StringBuilder sb = new StringBuilder();

                    for (String step : TreeView.this.steps) {
                        sb.append(step);
                        sb.append("\n");
                    }

                    Runnable r = new Runnable() {

                        @Override
                        public void run() {

                            JTextArea stepsTextArea = new JTextArea();

                            stepsTextArea.setFont(new Font("Monospaced", 0, 18));

                            stepsTextArea.setText(sb.toString());

                            JFrame f = new JFrame("Steps");
                            f.add(new JScrollPane(stepsTextArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

                            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                            f.setLocationByPlatform(true);
                            f.pack();
                            f.setMinimumSize(f.getSize());
                            f.setVisible(true);
                        }
                    };
                    SwingUtilities.invokeLater(r);

                }
            });
            buttonPanel.add(showStepsButton);
            

            if((Boolean) userOptions.get("Hints") == true)
            {
                nextStepButton = new JButton("Next Step");
                nextStepButton.addActionListener(new AbstractAction() {
                    public void actionPerformed(ActionEvent e) {

                        while (theUnknown == null || theUnknown.equals("_Null")) 
                        {
                            theUnknown = JOptionPane.showInputDialog("What is the unknown?");
                            if (!theUnknown.startsWith("_")) 
                            {
                                theUnknown = "_" + theUnknown;
                            }
                            
                            try 
                            {
                                Cons procedure = SublistCons.getInstance(AtomCons.getInstance(environment.getPrecision(), "FreeOf?"));
                                Cons currentExpression = TreeView.this.treePanel.getExpression(environment, -1);
                                Cons firstArgument = AtomCons.getInstance(environment.getPrecision(), theUnknown);
                                firstArgument.setCdr(currentExpression);
                                ((Cons) procedure.car()).setCdr(firstArgument);
                                Cons result = environment.iLispExpressionEvaluator.evaluate(environment, stackTop, procedure);
                                if (Utility.isTrue(environment, result, stackTop)) {
                                    JOptionPane.showMessageDialog(null, "The variable '" + theUnknown.replace("_", "") + "' does not occur in the expression.", "Message", JOptionPane.ERROR_MESSAGE);
                                    theUnknown = null;
                                }
                            } 
                            catch (Throwable t) 
                            {
                                t.printStackTrace();
                                JOptionPane.showMessageDialog(null, "There has been an internal error (11).", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        




                        try {
                            //Interpreter interpreter = SynchronousInterpreter.getInstance();
                            Cons currentExpression = TreeView.this.treePanel.getExpression(environment, stackTop);
                            Cons theUnknownCons = AtomCons.getInstance(environment.getPrecision(), theUnknown);
                            currentExpression.setCdr(theUnknownCons);
                            Cons head0 = SublistCons.getInstance(AtomCons.getInstance(environment.getPrecision(), "SolveSteps"));
                            ((Cons) head0.car()).setCdr(currentExpression);
                            if(isDisableOthersideRHS)
                            {
                                Cons colon = SublistCons.getInstance(AtomCons.getInstance(environment.getPrecision(), ":"));
                                Cons name = AtomCons.getInstance(environment.getPrecision(), "DisableOthersideRHS");
                                name.setCdr(Utility.getTrueAtom(environment));
                                ((Cons) colon.car()).setCdr(name);
                                theUnknownCons.setCdr(colon);
                            }
                            Cons result0 = environment.iLispExpressionEvaluator.evaluate(environment, stackTop, head0);
                            //EvaluationResponse response = interpreter.evaluate(head0);
                            //Cons result = response.getResultList();

                            int solutionStepsLength = Utility.listLength(environment, stackTop, ((Cons)result0.car()));

                            Cons firstStep = ((Cons)((Cons)result0.car()).cdr().cdr().car()).cdr();

                            Cons position = Utility.associationListGet(environment, stackTop, AtomCons.getInstance(environment.getPrecision(), "\"Position\""), firstStep);

                            if(position == null)
                            {
                                JOptionPane.showMessageDialog(null, "The solver cannot find a rule to apply to the expression.", "Error", JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                            
                            String positionString =  (String) Cons.caddar(position);
                            positionString = Utility.stripEndQuotesIfPresent(positionString);
                            
                            Cons ruleName = Utility.associationListGet(environment, stackTop, AtomCons.getInstance(environment.getPrecision(), "\"RuleName\""), firstStep);
                            String ruleNameString =  (String) Cons.caddar(ruleName);
                            ruleNameString = Utility.stripEndQuotesIfPresent(ruleNameString);
                            if(ruleNameString.equals("Otherside RHS"))
                            {
                                isDisableOthersideRHS = true;
                            }
                            
                            if(TreeView.this.subtreeCheckBox.isSelected())
                            {
                                treePanel.selectNode(positionString);
                            }

                            Cons manualSequenceCons = Utility.associationListGet(environment, stackTop, AtomCons.getInstance(environment.getPrecision(), "\"ManualSequence\""), firstStep);
                            String manualSequenceString =  (String) Cons.caddar(manualSequenceCons);

                            if(manualSequenceString.contains("None"))
                            {
                                JOptionPane.showMessageDialog(null, "The rule number was 'None'.", "Message", JOptionPane.INFORMATION_MESSAGE);
                            }
                            else
                            {
                                int manualSequence = Integer.parseInt(manualSequenceString);

                                if(TreeView.this.ruleCheckBox.isSelected())
                                {
                                    rulesPanel.selectRow(manualSequence);
                                }
                            }

                            if(solutionStepsLength == 3)
                            {
                                nextStepButton.setEnabled(false);
                            }
                        } catch (Throwable t) {
                            t.printStackTrace();
                            JOptionPane.showMessageDialog(null, "There has been an internal error (12).", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        
                        if(TreeView.this.subtreeCheckBox.isSelected() && TreeView.this.ruleCheckBox.isSelected())
                        {
                            TreeView.this.applyButton.setEnabled(true);
                        }
                    }
                });
                
                JPanel hintsPanel = new JPanel();
                hintsPanel.setBorder(BorderFactory.createTitledBorder("Hints"));
                hintsPanel.add(nextStepButton);
                
                subtreeCheckBox = new JCheckBox("Subtree");
                subtreeCheckBox.setSelected(true);
                subtreeCheckBox.addItemListener(new ItemListener() {
                    public void itemStateChanged(ItemEvent e) {
                        if(e.getStateChange() != ItemEvent.SELECTED)
                        {
                            TreeView.this.applyButton.setEnabled(false);
                        }
                    }
                });
                hintsPanel.add(subtreeCheckBox);
                
                
                ruleCheckBox = new JCheckBox("Rule");
                ruleCheckBox.setSelected(true);
                ruleCheckBox.addItemListener(new ItemListener() {
                    public void itemStateChanged(ItemEvent e) {
                        if(e.getStateChange() != ItemEvent.SELECTED)
                        {
                            TreeView.this.applyButton.setEnabled(false);
                        }
                    }
                });
                hintsPanel.add(ruleCheckBox);
                
                this.rulesPanel.addButtonRow2(hintsPanel, 0);
            }
        }

        
        showPositionsCheckBox.setSelected(false);

        showPositionsCheckBox.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.SELECTED) {
                    TreeView.this.treePanel.showPositions(true);
                } else {
                    TreeView.this.treePanel.showPositions(false);
                }
            }

        });
        
        
        showDepthsCheckBox.setSelected(false);

        showDepthsCheckBox.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.SELECTED) {
                    TreeView.this.treePanel.showDepths(true);
                } else {
                    TreeView.this.treePanel.showDepths(false);
                }
            }

        });
        
        

        if (includeSlider && includeExpression) {
            MathPanelController treePanelScaler = new MathPanelController(treePanel, viewScale);

            JScrollPane treeScrollPane = new JScrollPane(treeScreenCapturePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            treeScrollPane.getVerticalScrollBar().setUnitIncrement(16);

            treeBox.add(treeScrollPane);

            if (tabbedPane == null) {
                JSplitPane verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, latexBox, treeBox);
                panel.add(verticalSplitPane, BorderLayout.CENTER);
            } else {
                horizontalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,tabbedPane, treeBox);
                horizontalSplitPane.setOneTouchExpandable(true);
                //splitPane.setDividerLocation(150);
                JSplitPane verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, latexBox, horizontalSplitPane);
                panel.add(verticalSplitPane, BorderLayout.CENTER);
            }
            
            JPanel horizontalPanel = new JPanel();
            horizontalPanel.add(treePanelScaler);
            horizontalPanel.add(showPositionsCheckBox);
            horizontalPanel.add(showDepthsCheckBox);
            treeBox.add(horizontalPanel, BorderLayout.SOUTH);
        } else if (includeSlider) {
            MathPanelController treePanelScaler = new MathPanelController(treePanel, viewScale);

            JScrollPane treeScrollPane = new JScrollPane(treeScreenCapturePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            treeScrollPane.getVerticalScrollBar().setUnitIncrement(16);
            treeBox.add(treeScrollPane);
            if (tabbedPane == null) {
                panel.add(treeBox, BorderLayout.CENTER);
            } else {
                JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treeBox, tabbedPane);
                splitPane.setOneTouchExpandable(true);
                //splitPane.setDividerLocation(150);
                panel.add(splitPane, BorderLayout.CENTER);
            }
            
            JPanel horizontalPanel = new JPanel();
            horizontalPanel.add(treePanelScaler);
            horizontalPanel.add(showPositionsCheckBox);
            horizontalPanel.add(showDepthsCheckBox);
            treeBox.add(horizontalPanel, BorderLayout.SOUTH);
        } else if (includeExpression) {
            if (tabbedPane == null) {
                JSplitPane verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, latexBox, treeScreenCapturePanel);
                panel.add(verticalSplitPane, BorderLayout.CENTER);
            } else {
                horizontalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tabbedPane, treeScreenCapturePanel);
                horizontalSplitPane.setOneTouchExpandable(true);
                //splitPane.setDividerLocation(150);
                JSplitPane verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, latexBox, horizontalSplitPane);
                panel.add(verticalSplitPane, BorderLayout.CENTER);
            }
        } else {
            if (tabbedPane == null) {
                panel.add(treeScreenCapturePanel, BorderLayout.CENTER);
            } else {
                JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treeScreenCapturePanel, tabbedPane);
                splitPane.setOneTouchExpandable(true);
                //splitPane.setDividerLocation(150);
                panel.add(treeScreenCapturePanel, BorderLayout.CENTER);
            }
        }
        
        if(startPositionString != null)
        {
            final String startPositionStringFinal = startPositionString;
            final String endPositionStringFinal = endPositionString;
            SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     try
                     {
                        TreeView.this.treePanel.setIsSelectSingleNode(true);
                        TreeView.this.treePanel.selectPath(startPositionStringFinal, endPositionStringFinal);
                     }
                     catch(Throwable t)
                     {
                         t.printStackTrace();
                         JOptionPane.showMessageDialog(null, "There has been an internal error (13).", "Error", JOptionPane.ERROR_MESSAGE);
                     }
                }
            });
        }
        
        if(arcsHighlight)
        {
            TreeView.this.treePanel.setIsArcsHighlight(true);
        }
        
        if(nodesHighlight)
        {
            TreeView.this.treePanel.setIsNodesHighlight(true);
        }

        JavaObject response = null;
        
        if(returnList)
        {
            List responseList = new ArrayList();
            responseList.add(panel);
            responseList.add(treePanel);
            response = new JavaObject(responseList);
        }
        else
        {
           response = new JavaObject(panel); 
        }

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));

    }//end method.
    
    


    
    
    

    private void saveStep(Environment environment, int stackTop, Cons expression, String ruleName, String positionString) {

        if (positionString.equals("")) {
            positionString = "0";
        }
        
        if (positionString.equals(" ")) {
            positionString = "";
        }

        try {
            Cons head0 = SublistCons.getInstance(AtomCons.getInstance(environment.getPrecision(), "MetaToObject"));
            ((Cons) head0.car()).setCdr(expression);
            Cons result0 = environment.iLispExpressionEvaluator.evaluate(environment, stackTop, head0);

            TreeView.this.steps.add("[\"" + Utility.printMathPiperExpression(stackTop, result0, environment, 0, false, true, false) + "\", \"" + ruleName + "\", \"" + positionString + "\"],");
        } catch (Throwable t) {
            t.printStackTrace();
            JOptionPane.showMessageDialog(null, "There has been an internal error (14).", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void clearMetaInformation(Cons expression) throws Throwable {
        expression.setMetadataMap(null);

        Object object = expression.car();

        if (!(object instanceof Cons)) {
            return;
        }

        Cons cons = (Cons) object; // Go into sublist.

        cons.setMetadataMap(null);

        while (cons.cdr() != null) {

            cons = cons.cdr();

            if (cons instanceof SublistCons) {

                clearMetaInformation(cons);
            } else {
                cons.setMetadataMap(null);
            }
        }
    }

    
    public void highlightNode(Environment environment, int stackTop, Cons expression, String color) throws Throwable {
   
        Map metaDataMap = null;
        if (expression.getMetadataMap() == null) {
            metaDataMap = new HashMap();
            expression.setMetadataMap(metaDataMap);
        } else {
            metaDataMap = expression.getMetadataMap();
        }
        metaDataMap.put("\"HighlightColor\"", AtomCons.getInstance(environment.getPrecision(), "\"" + color + "\""));           
    }
        
        
    public void highlightTree(Environment environment, int stackTop, Cons expression, String color) throws Throwable {

        highlightNode(environment, stackTop, expression, color);
        
        if (expression instanceof SublistCons) {

            Cons cons = (Cons) expression.car(); // Go into sublist.

            highlightNode(environment, stackTop, cons, color);

            while (cons.cdr() != null) {
                cons = cons.cdr();

                highlightTree(environment, stackTop, cons, color);
            }
        }
    }

    public String expressionToLatex(Environment aEnvironment, int aStackTop, Cons expression) throws Throwable {
        //Evaluate Hold function.
        Cons holdAtomCons = AtomCons.getInstance(aEnvironment.getPrecision(), "Hold");
        holdAtomCons.setCdr(Cons.deepCopy(aEnvironment, aStackTop, expression));
        Cons holdSubListCons = SublistCons.getInstance(holdAtomCons);
        Cons holdInputExpression = holdSubListCons;

        //Obtain LaTeX version of the expression.
        Cons head = SublistCons.getInstance(AtomCons.getInstance(aEnvironment.getPrecision(), "UnparseLatex"));
        ((Cons) head.car()).setCdr(holdInputExpression);
        Cons result = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, head);
        String texString = (String) result.car();
        texString = Utility.stripEndQuotesIfPresent(texString);
        texString = texString.substring(1, texString.length());
        texString = texString.substring(0, texString.length() - 1);

        texString = texString.replace("$", "");
        texString = texString.replace("==", " =");

        return texString;
    }
    
    
private Cons expressionGet(Environment environment, int stackTop, Cons subExpression, String position) throws Throwable
{
    
    if(position.length() > 0)
    {
        String[] positionStrings = position.split(",");
        
        for(String positionString:positionStrings)
        {
            int positionInt = Integer.parseInt(positionString);
            subExpression = Utility.nth(environment, stackTop, subExpression, positionInt, false);
        }
    }
    
    return subExpression;
}


public void actionPerformed(ActionEvent ae)
{
    // RulesPanel event handler.
    try
    {
        treePanel.clearTree();

        // String ruleString = "'(" + ae.getActionCommand() + ");";

        String patternString = ae.getActionCommand();

        if(patternString.equals("="))
        {
            patternString = "==";
        }

        boolean isHold = true;
        boolean isObjectToMeta = true;

        if(environment.isOperator(patternString))
        {
            patternString = "ToAtom(\"" + patternString + "$\")";
            isHold = false;
            isObjectToMeta = false;
        }

        patternString += ";";

        StringInputStream newInput = new StringInputStream(patternString, environment.iInputStatus);

        Parser parser = new MathPiperParser(new MathPiperTokenizer(), newInput, environment, environment.iPrefixOperators, environment.iInfixOperators, environment.iPostfixOperators, environment.iBodiedProcedures);

        Cons patternCons = parser.parse(-1);

        if(isHold)
        {
            Cons holdAtomCons = AtomCons.getInstance(environment.getPrecision(), "Hold");
            holdAtomCons.setCdr(Cons.deepCopy(environment, -1, patternCons));
            patternCons = SublistCons.getInstance(holdAtomCons);
        }

        if(isObjectToMeta)
        {
            patternCons = Utility.applyString(environment, -1, "ObjectToMeta", patternCons);
        }


        Cons expression = treePanel.getExpression(environment, -1);

        expression.setCdr(patternCons);

        Cons highlightedExpression = Utility.applyString(environment, -1, "HighlightPattern", expression);

        treePanel.redrawTree(highlightedExpression);


        try {
            if(formula != null)
            {
                String latexString = expressionToLatex(environment, -1, highlightedExpression);
                formula.setLaTeX(latexString);
                TreeView.this.latexPanelController.adjust();
            }

        } catch (Throwable t) {
            t.printStackTrace();
            JOptionPane.showMessageDialog(null, "There has been an internal error (15).", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    catch (Throwable t)
    {
        t.printStackTrace();
        JOptionPane.showMessageDialog(null, "There has been an internal error (16).", "Error", JOptionPane.ERROR_MESSAGE);
    }
}
        
private class TreeViewPanel extends JPanel
{
    // Robot API.
    public void selectTabIndex(int index)
    {
        tabbedPane.setSelectedIndex(index - 1);
    }
    
    
    public List getComponentLocation(String name) throws Throwable
    {
        List list = TreeView.this.treeFeaturesPanel.getComponentLocation(name);
        
        return list;
    }
}
    
    

}//end class.

/*
 %mathpiper_docs,name="TreeView",categories="Programming Procedures,Visualization"
 *CMD TreeView --- display an expression tree

 *CALL
 TreeView(expression, option, option, option...)

 *PARMS
 {expression} -- an expression (which may be in string form) to display as an expression tree

 {Options:}

 {Scale} -- a value that sets the initial size of the tree

 {Resizable} -- if set to True, a resizing slider is displayed

 {IncludeExpression} -- if set to True, the algebraic form of the expression is included above the tree

 {Lisp} -- if set to True, the expression must be a string that is in Lisp form

 {Code} -- if set to True, the expression is rendered using code symbols instead of mathematical symbols


 *DESC
 Returns a Java GUI component that contains an expression rendered as an
 expression tree.

 Options are entered using the : operator.
 For example, here is how to disable {Resizable} option: {Resizable: False}.

 Right click on the images that are displayed to save them.
 
 *E.G.

 In> Show(TreeView( '(_a*(_b+_c) == _a*_b + _a*_c)))
 Result: java.awt.Component

 In> Show(TreeView( "_a*(_b+_c) == _a*_b + _a*_c"))
 Result: java.awt.Component

 In> Show(TreeView( "(+ 1 (* 2 3))", Lisp: True))
 Result: java.awt.Component


 *SEE Show
 %/mathpiper_docs
 */
