%mathpiper,def="Ceil"

5 # Ceil(Infinity) <-- Infinity;
5 # Ceil(-Infinity) <-- -Infinity;
5 # Ceil(Undefined) <-- Undefined;

10 # Ceil(x_RationalOrNumber?)
   <--
   {
     x:=NM(x);
     Local(prec,result,n);
     Assign(prec,BuiltinPrecisionGet());
     Decide(Zero?(x),Assign(n,2),
     Decide(x>?0,
       Assign(n,2+FloorN(NM(FastLog(x)/FastLog(10)))),
       Assign(n,2+FloorN(NM(FastLog(-x)/FastLog(10))))
       ));
     Decide(n>?prec,BuiltinPrecisionSet(n));
     Assign(result,CeilN(x));
     BuiltinPrecisionSet(prec);
     result;
   };
//   CeilN (NM(x));

%/mathpiper



%mathpiper_docs,name="Ceil",categories="Mathematics Functions;Numbers (Operations)"
*CMD Ceil --- round a number upwards
*STD
*CALL
        Ceil(x)

*PARMS

{x} -- a number

*DESC

This function returns $Ceil(x)$, the smallest integer larger than or equal to $x$.

*E.G.

In> Ceil(1.1)
Result: 2;

In> Ceil(-1.1)
Result: -1;

*SEE Floor, Round
%/mathpiper_docs