%mathpiper,def="OldCyclotomic;OldInternalCyclotomic"

// OldInternalCyclotomic(n,x,WantNormalForm) is the internal implementation
// WantNormalForm is a boolean parameter. If it is true, returns the normal
// form, if it is false returns the UniVariate representation.

// This (old) implementation makes use of the internal representations of univariate
// polynomials as UniVariate(var,begining,coefficients).
// There is also a version UniVariateCyclotomic(n,x) that returns the
// cyclotomic polynomial in the UniVariate representation.


10 # OldInternalCyclotomic(n_Even?,_x,WantNormalForm_Boolean?) <--
     {
      Local(k,m,p);
       k := 1;
       m := n;
        While(Even?(m))
       {
        k := k*2;
        m := m/2;
       };
       k := k/2 ;
       Decide(m>?1, {
                 p := OldInternalCyclotomic(m,x,False);
                 Decide(WantNormalForm, SubstituteAndExpandInUniVar(p,k),SubstituteInUniVar(p,k));
               },
                 Decide(WantNormalForm, x^k+1, UniVariateBinomial(x,k,1))
        );
     };

20 # OldInternalCyclotomic(n_Odd?,_x,WantNormalForm_Boolean?)_(n>?1) <--
{
 Local(divisors,poly1,poly2,q,d,f,result);
 divisors := MoebiusDivisorsList(n);
 poly1 :=1 ;
 poly2 := 1;
 ForEach (d,divisors)
 {
   q:=n/d[1];
   f:=UniVariateBinomial(x,q,-1);
   Decide(d[2]=?1,poly1:=poly1*f,poly2:=poly2*f);
 };
 result := Quotient(poly1,poly2);
 Decide(WantNormalForm,NormalForm(result),result);
};

10  # OldCyclotomic(1,_x) <-- _x-1;
20  # OldCyclotomic(n_Integer?,_x) <-- OldInternalCyclotomic(n,x,True);

%/mathpiper