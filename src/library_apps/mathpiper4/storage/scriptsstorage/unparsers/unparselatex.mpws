%mathpiper,def="UnparseLatex;UnparseLatexFinishList;UnparseLatexFinishList"

/* def file definitions
UnparseLatex
UnparseLatexMaxPrec
TexForm
*/

/* UnparseLatex: convert MathPiper objects to Latex math mode strings */

/* version 0.4 */

/* Changelog
        0.1        basic functionality
        0.2 fixed bracketing of Exp, added all infix ops and math functions
        0.3 fixed bracketing of lists, changed bracketing of math functions, modified Latex representation of user-defined functions (up to two-letter functions are in italics), added Latex Greek letters
        0.4 added nth roots, Sum, Limit, Integrate, hyperbolics, set operations, Abs, Max, Min, "==", ":=", Infinity; support indexed expressions A[i] and matrices.
        0.4.1 bugfixes for [] operator, support for multiple indices a[1][2][3]
        0.4.2 fix for variable names ending on digits "a2" represented as $a_2$
        0.4.3 bugfixes: complex I, indeterminate integration; relaxed bracketing of Sin()-like functions; implemented $TeX$ and $LaLatex$ correctly now (using \textrm{})
        0.4.4 use ordinary instead of partial derivative if expression has only one variable
        0.4.5 fixes for bracketing of Sum(); added <> to render as \sim and <=> to render as \approx; added BinomialCoefficient()
        0.4.6 moved the <> and <=> operators to initialization.rep/stdopers.mpi
        0.4.7 added Product() i.e. Product()
        0.4.8 added Differentiate(x,n), Deriv(x,n),  Implies? , and fixed errors with ArcSinh, ArcCosh, ArcTanh
        0.4.9 fixed omission: (fraction)^n was not put in brackets
        0.4.10 cosmetic change: insert \cdot between numbers in cases like 2*10^n
        0.4.11 added DumpErrors() to TexForm for the benefit of TeXmacs notebooks
        0.4.12 implement the % operation as Mod
        0.4.13 added Bessel{I,J,K,Y}, Ortho{H,P,T,U}, with a general framework for usual two-argument functions of the form $A_n(x)$; fix for Max, Min
        0.4.14 added mathematical notation for Floor(), Ceil()
        0.4.15 added Block() represented by ( )
        0.4.16 added Zeta()
*/

/* To do:
        0. Find and fix bugs.
        1. The current bracketing approach has limitations: can't omit extra brackets sometimes. " sin a b" is ambiguous, so need to do either "sin a sin b" or "(sin a) b" Hold((a*b)*Sqrt(x)). The current approach is *not* to bracket functions unless the enveloping operation is more binding than multiplication. This produces "sin a b" for both Sin(a*b) and Sin(a)*b but this is the current mathematical practice.
        2. Need to figure out how to deal with variable names such as "alpha3"
*/

//Retract("UnparseLatex", *);

/// TeXmacs unparser
//TexForm(_expr) <-- {DumpErrors();WriteString(UnparseLatex(expr));NewLine();};

RulebaseHoldArguments("UnparseLatex",[expression]);
RulebaseHoldArguments("UnparseLatex",[expression, precedence]);

/* Boolean predicate */


/* this function will put TeX brackets around the string if predicate holds */

Function ("UnparseLatexBracketIf", [predicate, string])
{
        Check(Boolean?(predicate) And? String?(string), "Argument", "UnparseLatex internal error: non-boolean and/or non-string argument of UnparseLatexBracketIf");
        Decide(predicate, ConcatStrings("( ", string, ") "), string);
};




Function ("UnparseLatexMatrixBracketIf", [predicate, string])
{
        Check(Boolean?(predicate) And? String?(string), "Argument", "UnparseLatex internal error: non-boolean and/or non-string argument of UnparseLatexMatrixBracketIf");
        Decide(predicate, ConcatStrings("\\left[ ", string, "\\right]"), string);
};



/* First, we convert UnparseLatex(x) to UnparseLatex(x, precedence). The enveloping precedence will determine whether we need to bracket the results. So UnparseLatex(x, UnparseLatexMaxPrec()) will always print "x", while UnparseLatex(x,-UnparseLatexMaxPrec()) will always print "(x)".
*/

UnparseLatexMaxPrec() := 60000;         /* This precedence will never be bracketed. It is equal to KMaxPrec */

/// main front-end
100 # UnparseLatex(_x) <-- ConcatStrings("$", UnparseLatex(x, UnparseLatexMaxPrec()), "$");

/* Replace numbers and variables -- never bracketed except explicitly */

110 # UnparseLatex(x_Number?, _p) <-- ToString(x);
/* Variables */
200 # UnparseLatex(x_Atom?, _p) <-- UnparseLatexLatexify(ToString(x));


/* Strings must be quoted but not bracketed */
100 # UnparseLatex(x_String?, _p) <--
{
    Local(characterList);

    characterList := [];
    ForEach(character, x)
    {
        Decide(character !=? " ", Append!(characterList, character), Append!(characterList, "\\hspace{2 mm}"));
    };
    ConcatStrings("\\mathrm{''", ListToString(characterList), "''}");
};



/* FunctionToList(...) can generate lists with atoms that would otherwise result in unparsable expressions. */
100 # UnparseLatex(x_Atom?, _p)_(Infix?(ToString(x))) <-- ConcatStrings("\\mathrm{", ToString(x), "}");


/* Lists: make sure to have matrices processed before them. Enveloping precedence is irrelevant because lists are always bracketed. List items are never bracketed. Note that UnparseLatexFinishList({a,b}) generates ",a,b" */

100 # UnparseLatex(x_List?, _p)_(Length(x) =? 0) <-- UnparseLatexBracketIf(True, "");
110 # UnparseLatex(x_List?, _p) <-- UnparseLatexBracketIf(True, ConcatStrings(UnparseLatex(First(x), UnparseLatexMaxPrec()), UnparseLatexFinishList(Rest(x)) ) );
100 # UnparseLatexFinishList(x_List?)_(Length(x) =? 0) <-- "";
110 # UnparseLatexFinishList(x_List?) <-- ConcatStrings(", ", UnparseLatex(First(x), UnparseLatexMaxPrec()), UnparseLatexFinishList(Rest(x)));

/* Replace operations */


        /* Template for "regular" binary infix operators:
100 # UnparseLatex(_x + _y, _p) <-- UnparseLatexBracketIf(p<?PrecedenceGet("+"), ConcatStrings(UnparseLatex(x, LeftPrecedenceGet("+")), " + ", UnparseLatex(y, RightPrecedenceGet("+")) ) );
        */
        
// special cases: things like x*2 and 2*10^x look ugly without a multiplication symbol
// cases like x*2
115 # UnparseLatex(_expr * n_Number?, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex(expr, LeftPrecedenceGet("*")), "\\times ", UnparseLatex(n, RightPrecedenceGet("*")) ) );
// cases like a*20! and a*10^x


116 # UnparseLatex(_n * _expr, _p) _ (Function?(expr) And? Contains?(["^", "!", "!!"], Type(expr)) And? Number?(FunctionToList(expr)[2])) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex(n, LeftPrecedenceGet("*")), "\\times ", UnparseLatex(expr, RightPrecedenceGet("*")) ) );

        /* generic binary ops here */
119 # UnparseLatex(expr_Function?, _p)_(ArgumentsCount(expr) =? 2 And? Infix?(Type(expr)) And? Not? Type(expr) =? "^" And? MetaGet(expr[0],"op") !=? Empty) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatex(FunctionToList(expr)[2], LeftPrecedenceGet(Type(expr))), "\\textcolor{Red}{", UnparseLatexLatexify(Type(expr)), "}", UnparseLatex(FunctionToList(expr)[3], RightPrecedenceGet(Type(expr))) ) );
120 # UnparseLatex(expr_Function?, _p)_(ArgumentsCount(expr) =? 2 And? Infix?(Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatex(FunctionToList(expr)[2], LeftPrecedenceGet(Type(expr))), UnparseLatexLatexify(Type(expr)), UnparseLatex(FunctionToList(expr)[3], RightPrecedenceGet(Type(expr))) ) );

        /* Template for "regular" unary prefix operators:
100 # UnparseLatex(+ _y, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("+"), ConcatStrings(" + ", UnparseLatex(y, RightPrecedenceGet("+")) ) );
        Note that RightPrecedenceGet needs to be defined for prefix ops or Else we won't be able to tell combined prefix/infix ops like "-" from strictly prefix ops.
        */

        /* generic unary ops here */
        /* prefix */
119 # UnparseLatex(expr_Function?, _p)_(ArgumentsCount(expr) =? 1 And? Prefix?(Type(expr)) And? MetaGet(expr[0],"op") !=? Empty) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings("\\textcolor{Red}{", UnparseLatexLatexify(Type(expr)), "}", UnparseLatex(FunctionToList(expr)[2], RightPrecedenceGet(Type(expr)))) );
120 # UnparseLatex(expr_Function?, _p)_(ArgumentsCount(expr) =? 1 And? Prefix?(Type(expr))) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatexLatexify(Type(expr)), UnparseLatex(FunctionToList(expr)[2], RightPrecedenceGet(Type(expr)))) );


        /* postfix */
120 # UnparseLatex(expr_Function?, _p)_(ArgumentsCount(expr) =? 1 And? Postfix?(Type(expr))) <-- UnparseLatexBracketIf(p <? LeftPrecedenceGet(Type(expr)), ConcatStrings(
        UnparseLatex(FunctionToList(expr)[2], LeftPrecedenceGet(Type(expr))),
        UnparseLatexLatexify(Type(expr))
) );

        /* fraction and its operands are never bracketed except when they are under power */
98 # UnparseLatex(_x / _y, _p)_(Assigned?(operatorMetaMap)) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\textcolor{Red}{\\frac \\textcolor{Black}{", UnparseLatex(x, UnparseLatexMaxPrec()), "} \\textcolor{Black}{", UnparseLatex(y, UnparseLatexMaxPrec()), "}} ") );
100 # UnparseLatex(_x / _y, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\frac{", UnparseLatex(x, UnparseLatexMaxPrec()), "}{", UnparseLatex(y, UnparseLatexMaxPrec()), "} ") );

        /* power's argument is never bracketed but it must be put in braces. Chained powers must be bracketed. Powers of 1/n are displayed as roots. */
100 # UnparseLatex(_x ^ (1/2), _p) <-- ConcatStrings("\\sqrt{", UnparseLatex(x, UnparseLatexMaxPrec()), "}");
101 # UnparseLatex(_x ^ (1/_y), _p) <-- ConcatStrings("\\sqrt[", UnparseLatex(y, UnparseLatexMaxPrec()), "]{", UnparseLatex(x, UnparseLatexMaxPrec()), "}");


120 # UnparseLatex(_x ^ _y, _p) <-- UnparseLatexBracketIf(p <=? PrecedenceGet("^"), ConcatStrings(UnparseLatex(x, PrecedenceGet("^")), " ^{", UnparseLatex(y, UnparseLatexMaxPrec()), "}" ) );

100 # UnparseLatex(If(_pred)_body, _p) <-- "\\textrm{if }(" ~ UnparseLatex(pred,60000) ~ ") " ~ UnparseLatex(body,60000);
100 # UnparseLatex(_left Else _right, _p) <-- UnparseLatex(left,60000) ~ "\\textrm{ Else }" ~ UnparseLatex(right,60000);


/* functions */


LocalSymbols(UnparseLatexRegularOps, UnparseLatexRegularPrefixOps, UnparseLatexGreekLetters, UnparseLatexSpecialNames) {

        /* addition, subtraction, multiplication, all comparison and logical operations are "regular" */

  UnparseLatexRegularOps :=
  [
    ["+"," + "],
    ["-"," - "],
    ["*"," \\times "],
    [":="," := "], //\\equiv "],
    ["=="," = "],
    ["=?"," = "],
    ["!=?","\\neq "],
    ["<=?","\\leq "],
    [">=?","\\geq "],
    ["<?"," < "],
    [">?"," > "],
    ["And?","\\wedge "],
    ["Or?", "\\vee "],
    ["<>", "\\sim "],
    ["<=>", "\\approx "],
    ["Implies?", "\\Rightarrow "],
    ["Equivales?", "\\equiv "],
    ["%", "\\bmod "],
  ];

  UnparseLatexRegularPrefixOps := [ ["+"," + "], ["-"," - "], ["Not?"," \\neg "] ];



    /* Unknown function: precedence 200. Leave as is, never bracket the function itself and bracket the argumentPointer(s) automatically since it's a list. Other functions are precedence 100 */

  UnparseLatexGreekLetters := ["Gamma", "Delta", "Theta", "Lambda", "Xi", "Pi", "Sigma", "Upsilon", "Phi", "Psi", "Omega", "alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta", "iota", "kappa", "lambda", "mu", "nu", "xi", "pi", "rho", "sigma", "tau", "upsilon", "phi", "chi", "psi", "omega", "varpi", "varrho", "varsigma", "varphi", "varepsilon"];
  UnparseLatexSpecialNames := [
    ["I", "\\imath "],        // this prevents a real uppercase I, use BesselI instead
    ["Pi", "\\pi "],        // this makes it impossible to have an uppercase Pi... hopefully it's not needed
    ["Infinity", "\\infty "],
    ["TeX", "\\textrm{\\TeX\\/}"],
    ["LaTeX", "\\textrm{\\LaTeX\\/}"],
    ["Maximum", "\\max "],        // this replaces these function names
    ["Minimum", "\\min "],
    ["Block", " "],
    ["Zeta", "\\zeta "],
  ];


  /* this function will take a user-defined variable or function name and output either this name unmodified if it's only 2 characters long, or the name in normal text if it's longer, or a Latex Greek letter code */
  Function ("UnparseLatexLatexify", [string])
  {
    Check(String?(string), "Argument", "UnparseLatex internal error: non-string argument of UnparseLatexLatexify");
    /* Check if it's a greek letter or a special name */
    Decide(Contains?(AssocIndices(UnparseLatexSpecialNames), string), UnparseLatexSpecialNames[string],
    Decide(Contains?(UnparseLatexGreekLetters, string), ConcatStrings("\\", string, " "),
    Decide(Contains?(AssocIndices(UnparseLatexRegularOps), string), UnparseLatexRegularOps[string],
    Decide(Contains?(AssocIndices(UnparseLatexRegularPrefixOps), string), UnparseLatexRegularPrefixOps[string],
    Decide(Length(string) >=? 2 And? Number?(ToAtom(StringMidGet(2, Length(string)-1, string))), ConcatStrings(StringMidGet(1,1,string), "_{", StringMidGet(2, Length(string)-1, string), "}"),
    Decide(Length(string) >? 2, ConcatStrings("\\mathrm{ ", string, " }"),
    string
    ))))));
  };

};

/* */

/* Unknown bodied function */

200 # UnparseLatex(x_Function?, _p) _ (Bodied?(Type(x))) <-- {
        Local(func, args, lastarg);
        func := Type(x);
        args := Rest(FunctionToList(x));
        lastarg := PopBack(args);
        UnparseLatexBracketIf(p <? PrecedenceGet(func), ConcatStrings(
          UnparseLatexLatexify(func), UnparseLatex(args, UnparseLatexMaxPrec()),  UnparseLatex(lastarg, PrecedenceGet(func))
        ));
};

/* Unknown infix function : already done above
210 # UnparseLatex(x_Function?, _p)_(Infix?(Type(x))) <-- ConcatStrings(UnparseLatexLatexify(Type(x)), UnparseLatex(Rest(FunctionToList(x)), UnparseLatexMaxPrec()) );
*/
/* Unknown function that is not prefix, infix or postfix */
220 # UnparseLatex(x_Function?, _p) <-- ConcatStrings(UnparseLatexLatexify(Type(x)), UnparseLatex(Rest(FunctionToList(x)), UnparseLatexMaxPrec()) );

        /* Never bracket Sqrt or its arguments */
100 # UnparseLatex(Sqrt(_x), _p) <-- ConcatStrings("\\sqrt{", UnparseLatex(x, UnparseLatexMaxPrec()), "}");

        /* Always bracket Exp's arguments */
100 # UnparseLatex(Exp(_x), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\exp ", UnparseLatexBracketIf(True, UnparseLatex(x, UnparseLatexMaxPrec())) ) );


LocalSymbols(UnparseLatexMathFunctions, UnparseLatexMathFunctions2) {

  /* Sin, Cos, etc. and their argument is bracketed when it's a sum or product but not bracketed when it's a power. */
  /// supported MathPiper functions: "mathematical" functions of one argument which sometimes do not require parentheses (e.g. $\sin x$ )
  UnparseLatexMathFunctions := [ ["Cos","\\cos "], ["Sin","\\sin "], ["Tan","\\tan "], ["Cosh","\\cosh "], ["Sinh","\\sinh "], ["Tanh","\\tanh "], ["Ln","\\ln "], ["ArcCos","\\arccos "], ["ArcSin","\\arcsin "], ["ArcTan","\\arctan "], ["ArcCosh","\\mathrm{arccosh}\\, "], ["ArcSinh","\\mathrm{arcsinh}\\, "], ["ArcTanh","\\mathrm{arctanh}\\, "],
  ["Erf", "\\mathrm{erf}\\, "], ["Erfc", "\\mathrm{erfc}\\, "],
  ];

  /// supported MathPiper functions: functions of two arguments of the form $A_n(x)$
  UnparseLatexMathFunctions2 := [
  ["BesselI", "I "], ["BesselJ", "J "],
  ["BesselK", "K "], ["BesselY", "Y "],
  ["OrthoH", "H "], ["OrthoP", "P "],
  ["OrthoT", "T "], ["OrthoU", "U "],
  ];

  // generic two-argument functions of the form $A(x,y)$ where just the name has to be changed: handle this using the usual naming conversion scheme (UnparseLatexSpecialNames)

  /* Precedence of 120 because we'd like to process other functions like sqrt or exp first */

  // generic math functions of one argument
  120 # UnparseLatex(expr_Function?, _p) _ (ArgumentsCount(expr) =? 1 And? Contains?(AssocIndices(UnparseLatexMathFunctions), Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatexMathFunctions[Type(expr)], UnparseLatex( FunctionToList(expr)[2], PrecedenceGet("*")) ) );

  /// math functions two arguments of the form $A_n(x)$
  120 # UnparseLatex(expr_Function?, _p) _ (ArgumentsCount(expr) =? 2 And? Contains?(AssocIndices(UnparseLatexMathFunctions2), Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"),
    ConcatStrings(
    UnparseLatexMathFunctions2[Type(expr)],
    "_{",
    UnparseLatex( FunctionToList(expr)[2], UnparseLatexMaxPrec()),        // never bracket the subscript
    "}",
    UnparseLatexBracketIf(True, UnparseLatex(FunctionToList(expr)[3], UnparseLatexMaxPrec()) ) // always bracket the function argument
    )
  );

}; // LocalSymbols(UnparseLatexMathFunctions, UnparseLatexMathFunctions2)


/* Complex numbers */
100 # UnparseLatex(Complex(0, 1), _p) <-- UnparseLatex(Hold(I), p);
100 # UnparseLatex(Complex(_x, 0), _p) <-- UnparseLatex(x, p);
110 # UnparseLatex(Complex(_x, 1), _p) <-- UnparseLatex(x + Hold(I), p);
110 # UnparseLatex(Complex(0, _y), _p) <-- UnparseLatex(Hold(I)*y, p);
120 # UnparseLatex(Complex(_x, _y), _p) <-- UnparseLatex(x + Hold(I)*y, p);

/* Abs(), Floor(), Ceil() are displayed as special brackets */

100 # UnparseLatex(Abs(_x), _p) <-- ConcatStrings("\\left| ", UnparseLatex(x, UnparseLatexMaxPrec()), "\\right| ");
100 # UnparseLatex(Floor(_x), _p) <-- ConcatStrings("\\left\\lfloor ", UnparseLatex(x, UnparseLatexMaxPrec()), "\\right\\rfloor ");
100 # UnparseLatex(Ceil(_x), _p) <-- ConcatStrings("\\left\\lceil ", UnparseLatex(x, UnparseLatexMaxPrec()), "\\right\\rceil ");

/* Some functions which are displayed as infix: Mod, Union, Intersection, Difference, Contains? */

100 # UnparseLatex(Modulo(_x, _y), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex(x, PrecedenceGet("/")), "\\bmod ", UnparseLatex(y, PrecedenceGet("/")) ) );

100 # UnparseLatex(Union(_x, _y), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex(x, PrecedenceGet("/")), "\\cup ", UnparseLatex(y, PrecedenceGet("/")) ) );

100 # UnparseLatex(Intersection(_x, _y), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex(x, PrecedenceGet("/")), "\\cap ", UnparseLatex(y, PrecedenceGet("/")) ) );

100 # UnparseLatex(Difference(_x, _y), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex(x, PrecedenceGet("/")), "\\setminus ", UnparseLatex(y, PrecedenceGet("/")) ) );

/* "Contains?" is displayed right to left */

100 # UnparseLatex(Contains?(_x, _y), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex(y, PrecedenceGet("/")), "\\in ", UnparseLatex(x, PrecedenceGet("/")) ) );

/// Binomial coefficients: always bracketed
100 # UnparseLatex(BinomialCoefficient(_n, _m), _p) <-- UnparseLatexBracketIf(False, ConcatStrings("{", UnparseLatex(n, UnparseLatexMaxPrec()), " \\choose ", UnparseLatex(m, UnparseLatexMaxPrec()), "}" )
);

/* Some functions with limits: Lim, Sum, Integrate */

100 # UnparseLatex(Sum(_x, _x1, _x2, _expr), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\sum _{", UnparseLatex(x, UnparseLatexMaxPrec()), " = ", UnparseLatex(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex(x2, UnparseLatexMaxPrec()), "} ", UnparseLatex(expr, PrecedenceGet("*")) ) );

100 # UnparseLatex(Sum(_expr), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\sum ", UnparseLatex(expr, PrecedenceGet("*")) ) );


100 # UnparseLatex(Product(_x, _x1, _x2, _expr), _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\prod _{", UnparseLatex(x, UnparseLatexMaxPrec()), " = ", UnparseLatex(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex(x2, UnparseLatexMaxPrec()), "} ", UnparseLatex(expr, PrecedenceGet("*")) ) );

100 # UnparseLatex(Integrate(_x, _x1, _x2) _expr, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int _{", UnparseLatex(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex(x2, UnparseLatexMaxPrec()), " } ", UnparseLatex(expr, PrecedenceGet("*")), " d", UnparseLatex(x, UnparseLatexMaxPrec())
) );

/* indeterminate integration */
100 # UnparseLatex(Integrate(_x) _expr, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int ", UnparseLatex(expr, PrecedenceGet("*")), " d", UnparseLatex(x, UnparseLatexMaxPrec())
) );

100 # UnparseLatex(Limit(_x, _x1) _expr, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\lim _{", UnparseLatex(x, UnparseLatexMaxPrec()), "\\rightarrow ", UnparseLatex(x1, UnparseLatexMaxPrec()), "} ", UnparseLatex(expr, PrecedenceGet("/")) ) );

/* Derivatives */

/* Use partial derivative only when the expression has several variables */
100 # UnparseLatex(Deriv(_x)_y, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("-"), ConcatStrings(
        Decide(Length(VarList(y))>?1, "\\frac{\\partial}{\\partial ", "\\frac{d}{d "
        ), UnparseLatex(x, PrecedenceGet("^")), "}", UnparseLatex(y, PrecedenceGet("/")) ) );

100 # UnparseLatex(Deriv(_x, _n)_y, _p) <-- UnparseLatexBracketIf(p <? PrecedenceGet("-"), ConcatStrings(
        Decide(
                Length(VarList(y))>?1,
                "\\frac{\\partial^" ~ UnparseLatex(n, UnparseLatexMaxPrec()) ~ "}{\\partial ",
                "\\frac{d^" ~ UnparseLatex(n, UnparseLatexMaxPrec()) ~ "}{d "
        ), UnparseLatex(x, PrecedenceGet("^")), " ^", UnparseLatex(n, UnparseLatexMaxPrec()), "}", UnparseLatex(y, PrecedenceGet("/")) ) );
100 # UnparseLatex(Differentiate(_x)_y, _p) <-- UnparseLatex(Deriv(x) y, p);
100 # UnparseLatex(Differentiate(_x, _n)_y, _p) <-- UnparseLatex(Deriv(x, n) y, p);

/* Indexed expressions */

/* This seems not to work because x[i] is replaced by Nth(x,i) */
/*
100 # UnparseLatex(_x [ _i ], _p) <-- ConcatStrings(UnparseLatex(x, UnparseLatexMaxPrec()), " _{", UnparseLatex(i, UnparseLatexMaxPrec()), "}");
*/
/* Need to introduce auxiliary function, or Else have trouble with arguments of Nth being lists */
RulebaseHoldArguments("UnparseLatexNth",[x,y]);
100 # UnparseLatex(Nth(Nth(_x, i_List?), _j), _p) <-- UnparseLatex(UnparseLatexNth(x, Append(i,j)), p);
100 # UnparseLatex(UnparseLatexNth(Nth(_x, i_List?), _j), _p) <-- UnparseLatex(UnparseLatexNth(x, Append(i,j)), p);
110 # UnparseLatex(Nth(Nth(_x, _i), _j), _p) <-- UnparseLatex(UnparseLatexNth(x, List(i,j)), p);
120 # UnparseLatex(Nth(_x, _i), _p) <-- ConcatStrings(UnparseLatex(x, UnparseLatexMaxPrec()), " _{", UnparseLatex(i, UnparseLatexMaxPrec()), "}");
120 # UnparseLatex(UnparseLatexNth(_x, _i), _p) <-- ConcatStrings(UnparseLatex(x, UnparseLatexMaxPrec()), " _{", UnparseLatex(i, UnparseLatexMaxPrec()), "}");

/* Matrices are always bracketed. Precedence 80 because lists are at 100. */

80 # UnparseLatex(M_Matrix?, _p) <-- UnparseLatexMatrixBracketIf(True, UnparseLatexPrintMatrix(M));

Function ("UnparseLatexPrintMatrix", [M])
{
/*
        Want something like "\begin{array}{cc} a & b \\ c & d \\ e & f \end{array}"
        here, "cc" is alignment and must be given for each column
*/
        Local(row, col, result, ncol);
        result := "\\begin{array}{";
        ForEach(col, M[1]) result:=ConcatStrings(result, "c");
        result := ConcatStrings(result, "}");

        ForEach(row, 1 .. Length(M)) {
                ForEach(col, 1 .. Length(M[row])) {
                        result := ConcatStrings( result, " ", UnparseLatex(M[row][col], UnparseLatexMaxPrec()), Decide(col =? Length(M[row]), Decide(row =? Length(M), "", " \\\\"), " &"));
                };
        };

        ConcatStrings(result, " \\end{array} ");
};

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="UnparseLatex",categories="Programming Functions;Input/Output"
*CMD UnparseLatex --- export expressions to $LaTeX$
*STD
*CALL
        UnparseLatex(expr)

*PARMS

{expr} -- an expression to be exported

*DESC

{UnparseLatex} returns a string containing a $LaTeX$ representation 
of the MathPiper expression {expr}. Currently the exporter handles 
most expression types but not all.

*E.G.

In> UnparseLatex(Sin(a1)+2*Cos(b1))
Result: "\$\sin a_{1} + 2 \cos b_{1}\$";

*SEE UnparseMath2D, UnparseC
%/mathpiper_docs





%mathpiper,name="UnparseLatex",subtype="automatic_test"

/* it worketh no more...
Testing("Realistic example");
f:=Exp(I*lambda*eta)*w(T*(k+k1+lambda));
g:=Simplify(Substitute(lambda,0) f+(k+k1)*(Differentiate(lambda)f)+k*k1*Differentiate(lambda)Differentiate(lambda)f );
Verify(UnparseLatex(g), ...);
*/

Verify(
UnparseLatex(Hold(Cos(A-B)*Sqrt(C+D)-(a+b)*c^d+2*I+Complex(a+b,a-b)/Complex(0,1)))
,"$\\cos ( A - B)  \\times \\sqrt{C + D} - ( a + b)  \\times c ^{d} + 2 \\times \\imath  + \\frac{a + b + \\imath  \\times ( a - b) }{\\imath } $"
);

Verify(
UnparseLatex(Hold(Exp(A*B)/C/D/(E+F)*G-(-(a+b)-(c-d))-b^(c^d) -(a^b)^c))
,"$\\frac{\\frac{\\frac{\\exp ( A \\times B) }{C} }{D} }{E + F}  \\times G - (  - ( a + b)  - ( c - d) )  - b ^{c ^{d}} - ( a ^{b})  ^{c}$"
);

Verify(
UnparseLatex(Hold(Cos(A-B)*Sin(a)*f(b,c,d*(e+1))*Sqrt(C+D)-(g(a+b)^(c+d))^(c+d)))
,"$\\cos ( A - B)  \\times \\sin a \\times f( b, c, d \\times ( e + 1) )  \\times \\sqrt{C + D} - ( g( a + b)  ^{c + d})  ^{c + d}$"
);


/* This test is commented out because it throws an exception when orthopoly.mpws is removed from the build process.
// testing latest features: \\times, %, (a/b)^n, BinomialCoefficient(), BesselI, OrthoH
Verify(
UnparseLatex(3*2^n+Hold(x*10!) + (x/y)^2 + BinomialCoefficient(x,y) + BesselI(n,x) + Maximum(a,b) + OrthoH(n,x))
, "$3\\times 2 ^{n} + x\\times 10! + ( \\frac{x}{y} )  ^{2} + {x \\choose y} + I _{n}( x)  + \\max ( a, b)  + H _{n}( x) $"
);
*/

/* this fails because of a bug that Differentiate(x) f(y) does not go to 0 */ /*
Verify(
UnparseLatex(3*Differentiate(x)f(x,y,z)*Cos(Omega)*Modulo(Sin(a)*4,5/a^b))
,"$3 ( \\frac{\\partial}{\\partial x}f( x, y, z) )  ( \\cos \\Omega )  ( 4 ( \\sin a) ) \\bmod \\frac{5}{a ^{b}} $"
);
*/


RulebaseHoldArguments("f",[x]);
Verify(
UnparseLatex(Hold(Differentiate(x)f(x)))
,"$\\frac{d}{d x}f( x) $");
Retract("f",1);

Verify(
UnparseLatex(Hold(Not? (c <? 0) And? (a+b)*c>=? -d^e And? (c <=? 0 Or? b+1 >? 0) Or? a !=? 0 And? Not? (p =? q)))
,"$ \\neg c < 0\\wedge ( a + b)  \\times c\\geq  - d ^{e}\\wedge ( c\\leq 0\\vee b + 1 > 0) \\vee a\\neq 0\\wedge  \\neg p = q$"
);

RulebaseHoldArguments("f",[x,y,z]);
Verify(
UnparseLatex((Differentiate(x)f(x,y,z))*Cos(Omega)*Modulo(Sin(a)*4,5/a^b))
,"$( \\frac{\\partial}{\\partial x}f( x, y, z) )  \\times \\cos \\Omega  \\times ( 4 \\times \\sin a) \\bmod \\frac{5}{a ^{b}} $"
);
Retract("f",3);

RulebaseHoldArguments("g",[x]);
RulebaseHoldArguments("theta",[x]);
Verify(
UnparseLatex(Pi+Exp(1)-Theta-Integrate(x,x1,3/g(Pi))2*theta(x)*Exp(1/x))
,"$\\pi  + \\exp ( 1)  - \\Theta  - \\int _{x_{1}} ^{\\frac{3}{g( \\pi ) }  } 2 \\times \\theta ( x)  \\times \\exp ( \\frac{1}{x} )  dx$"
);
Retract("g",1);
Retract("theta",1);

Verify(
UnparseLatex([a[3]*b[5]-c[1][2],[a,b,c,d]])
,"$( a _{3} \\times b _{5} - c _{( 1, 2) }, ( a, b, c, d) ) $"
);


//Note: this is the only code in the test suite that currently creates new rulebases.
RulebaseHoldArguments("aa",[x,y,z]);
Bodied("aa", 200);
RulebaseHoldArguments("bar", [x,y]);
Infix("bar", 100);
Verify(
UnparseLatex(aa(x,y) z + 1 bar y!)
,"$aa( x, y) z + 1\\mathrm{ bar }y!$"
);
Retract("aa",3);
Retract("bar",2);

Verify(
UnparseLatex(x^(1/3)+x^(1/2))
, "$\\sqrt[3]{x} + \\sqrt{x}$"
);

/*
Verify(
UnparseLatex()
,""
);
*/

/* Bug report from Michael Borcherds. The brackets were missing. */
Verify(UnparseLatex(Hold(2*x*(-2))), "$2 \\times x \\times (  - 2) $");


%/mathpiper