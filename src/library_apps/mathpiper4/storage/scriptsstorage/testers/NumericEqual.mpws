%mathpiper,def="NumericEqual"

//Retract("NumericEqual",*);

10 # NumericEqual(left_Decimal?, right_Decimal?, precision_PositiveInteger?) <--
{
    Decide(InVerboseMode(),Tell("NumericEqual",[left,right]));
    Local(repL,repR,precL,precR,newL,newR,plo,phi,replo,rephi);
    Local(newhi,newrepL,newlo,newrepR,ans);
    repL     := NumberToRep(left);
    repR     := NumberToRep(right);
    precL    := repL[2];
    precR    := repR[2];
    Decide(InVerboseMode(),Tell("  ",[precL,precR,precision]));
    newL     := RoundToPrecision(left,  precision );
    newR     := RoundToPrecision(right, precision );
    Decide(InVerboseMode(),Tell("  ",[newL,newR]));
    newrepL  := NumberToRep( newL );
    newrepR  := NumberToRep( newR );
    Decide(InVerboseMode(),Tell("  ",[newrepL,newrepR]));
    ans      := Verify( newrepL[1] - newrepR[1], 0 );
    Decide(InVerboseMode(),Tell("        ",ans));
    ans;
};


15 # NumericEqual(left_Integer?, right_Integer?, precision_PositiveInteger?) <--
{
    Decide(InVerboseMode(),Tell("NumericEqualInt",[left,right]));
    left =? right;
};


20 # NumericEqual(left_Number?, right_Number?, precision_PositiveInteger?) <--
{
    Decide(InVerboseMode(),Tell("NumericEqualNum",[left,right]));
    Local(nI,nD,repI,repD,precI,precD,intAsDec,newDec,newrepI,newrepD,ans);
    Decide( Integer?(left), {nI:=left; nD:=right;}, {nI:=right; nD:=left;});
    // the integer can be converted to the equivalent decimal at any precision
    repI  := NumberToRep(nI);
    repD  := NumberToRep(nD);
    precI := repI[2];
    precD := repD[2];
    intAsDec := RoundToPrecision(1.0*nI,precision);
    newDec   := RoundToPrecision( nD,   precision );
    newrepI  := NumberToRep( intAsDec );
    newrepD  := NumberToRep( newDec   );
    Decide(InVerboseMode(),
      {
          Tell("        ",[nI,nD]);
          Tell("    ",[repI,repD]);
          Tell("  ",[precI,precD]);
          Tell("       ",[intAsDec,newDec]);
          Tell("       ",[newrepI,newrepD]);
       }
    );
    ans      := Verify( newrepI[1] - newrepD[1], 0 );
    Decide(InVerboseMode(),Tell("        ",ans));
    ans;
};


25 # NumericEqual(left_Complex?, right_Complex?, precision_PositiveInteger?) <--
{
    Decide(InVerboseMode(),Tell("NumericEqualC",[left,right]));
    Local(rrL,iiL,rrR,iiR,ans);
    rrL := Re(left);
    iiL := Im(left);
    rrR := Re(right);
    iiR := Im(right);
    Decide(InVerboseMode(),
      {
         Tell("  ",[left,right]);
         Tell("  ",[rrL,rrR]);
         Tell("  ",[iiL,iiR]);
      }
    );
    ans := (NumericEqual(rrL,rrR,precision) And? NumericEqual(iiL,iiR,precision));
};

%/mathpiper


